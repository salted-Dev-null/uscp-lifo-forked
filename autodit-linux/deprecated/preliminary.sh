#!/bin/bash

# NOTE TO DEVS:
# DOCUMENTATION PENDING!
# PENDING DEBUG! 
# checks use sha256sum, which, type, whereis

# URGENT DEBUG:
# SHA256 SIGNATURES ARE NOT RELIABLE AS UPDATES MIGHT BREAK THEM!
# NEED TO FIND A WAY TO SATISFY SCRIPT COMMANDS WITH SAFE BINARY EXECUTABLE!

# Credits:
#	Script is created by Jaeger105, mostly referencing NDG Linux, rkhunter, 
#	chkrootkit and lynis, for CP-XI team LIFO.



# Table of contents: (search to jump!)
#	check root
#	define variables
#	check core binaries	(PENDING DEBUG! SEE DEV NOTES!)
#	check apt sources
#	check $PATH variable
#	check using whereis



# ------ check root ------
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



# ------ define variables ------
correctSums="97b38f828430a8df358b084685035792efba729107a0736be70613e25dae763c  /usr/sbin/adduser
1f2359fad1327cb884845dc4e974eaf9e6290a2fd45d1a9efc58c33972193a60  /usr/sbin/chroot
3aebb0ced6211d4b6b021505caacb35b25b1b11dc125ae3391082368b7243b2c  /usr/sbin/cron
7274989b6b8e7ac8201b85139ed6b32fe2f9c8cc7313e38d2c12c9eee2fa5171  /usr/sbin/groupadd
c48d32fe2f4959167bd6bfc688c3cf29c2fcd2a6be9309114a0c6fa4422cd9d8  /usr/sbin/groupdel
af3e688333f0d859c7447f725567aad7ab9c763dcde90b9defb84aec4d84e1f2  /usr/sbin/groupmod
8a6407b091487d2a30b52e69f15d8c1d5d873904b77c334c150deb0274e4583c  /usr/sbin/grpck
b6a40cf6f883aa3f5042e54a4e7f455846e983fb3b1769caa580139cb4a0107f  /usr/sbin/nologin
2ee9608b222cf4ef2d8b1023a85f754d0a9bc0a07173634237ccf8ce0bbf0c14  /usr/sbin/pwck
fc6c00894941937aa45981b8cdd9f3714cda90b827fe5ce89f794e2ad0c212d9  /usr/sbin/rsyslogd
c8077e384aaeeb998b4af43b94783875778b7ee79d79406ec27af9a5148cb3ae  /usr/sbin/useradd
998156c0f1d53831a978ac8c1c7a0dcdf18f1f01e59eb38c0540a5db7b759595  /usr/sbin/userdel
506e6ec1591e30a8b4084713a438955d411faf9362f54937c3cd19775c90c793  /usr/sbin/usermod
9ad29fc75e4804f85e027c7e9ecb4979da402438ec3a098829aa74cf0a5a0c72  /usr/sbin/vipw
42679c32d6e33638a66a232ff4c56fd2864be016380c6a8d7df6c6f0695a8e30  /usr/bin/awk
4ac8ddcdc3d31992dd18c2e6ef929d9a79f1691d1f16e17c5d2234de886e90eb  /usr/bin/basename
4bc88abb911956c5eba1837e8d3cb0a0240b8c0088cb8dd127baa7720d2e06a4  /usr/bin/chattr
93e226f3876bb17cf76954bf7ac203adf6a948c81ca82941f4957d69f8e1a9bd  /usr/bin/cut
1efbc12a210ebbd9b7f90eec4c4f53bf314d9680839622d9c73143891439f3b2  /usr/bin/diff
7f431a680803ff5a1dff2fbf5da0552880c5b1d5543f3171a53505a1cde668c5  /usr/bin/dirname
4cd1785c8f89501a464beac4a166b8dc8a626f479bf515af32bb7e9fc9840f8e  /usr/bin/dpkg
6ec064d94e2c44f841292fddb07a6f5110ce61f4b68a38a109ccc1d53f5ca756  /usr/bin/dpkg-query
24574fdc99ed01b1d2417541586ce097567f382faaf32b43015f2c2cdb99043c  /usr/bin/du
6b9ca1b82772aa6aa225af0ade62e098bb25ba761f36188c12101260779c5815  /usr/bin/env
e97ab1817c17511cb7cf3110997ceccb4baa587a7346ffb36fb2d103eb88f452  /usr/bin/file
189fbf2416c8205430d8eaa85e2947bc15504ca335ad4a77ec668ff3cbf9c84a  /usr/bin/find
019e12ce7fb4adf89ca485ffa54566b8529d824caa8a2644fa89208dae6f78e9  /usr/bin/GET
1d75c26680a858748e4a99472403140cf21aeaaaf644c2413d32e604df84592a  /usr/bin/groups
d10d2c03eb3aa9cd7d7cba22205bbfce3bdd623c2c7e3fced895329afc914d1d  /usr/bin/head
8135463b5a74a4a0777f4946545770814d5c929a6661b2041aee78e8458146f1  /usr/bin/id
4b91575d65bd4b44c300a55c7c7474a5c4f158b72b7050d5bb7c094e030ef560  /usr/bin/ipcs
525efa977202c43ea5d8ce0d86a42bb34be77175d8ad066e00cba374e397074f  /usr/bin/killall
5ed758c30eb9db085fb0736e001463595e48600f71d15b956309ce35f9355a09  /usr/bin/last
90e9cdc574cd27261350582c05b883deff0f1430144c6619724b361bc566565b  /usr/bin/lastlog
67d2cbcfd0c992381283abd2e8ed6f1f74b76ddb011e704499e3ac5a8ac71f12  /usr/bin/ldd
d5a7d454ed6305fb65dafb5f552521a051df1cb09dd2cd383a63922857f8715f  /usr/bin/less
56ca7bc6db6f168b7c29870a4f8e9f7dff5e09ebbcf8a5a9d704c5c3816a1b43  /usr/bin/locate
31581aeddd9b97d5fd22c0576ed602c170dc6c682703f9110324f4dc1d2d7103  /usr/bin/logger
20b05f6fea4561c6e04095a38e6c1bca733f05222db317f2386f010452c180b9  /usr/bin/lsattr
1190f4dbc7be174de8fd4096c9bf7a28eebfac937d308b7cc533be4a1240d26e  /usr/bin/lsof
c7a7992f05f7b8a65b9bd8064a396a31df230649c12ac7a7989282688bf39dc2  /usr/bin/mail
331461536894ebf97e5d4115fc3ec4f33b207f3d2dde380adfdfc4edd8a258d2  /usr/bin/md5sum
56ca7bc6db6f168b7c29870a4f8e9f7dff5e09ebbcf8a5a9d704c5c3816a1b43  /usr/bin/mlocate
a62482d823e335c9b113f78ddbe58d8d5561aea260f713f4cbf49bdb9e3e8f93  /usr/bin/newgrp
74d2ffc34d86ace2f5d9dabb95de8c75abe824cd6cfc871d89faf65696241d2e  /usr/bin/passwd
bb206ce5ddccbb2f070b0e46f584c07bc22dd050c308e47eb7e0b55a40afba0e  /usr/bin/perl
52086dbdb63bf01bc6c247470a895ac1925bbd1de6452a256cdbaaa48d8e3ef7  /usr/bin/pgrep
52086dbdb63bf01bc6c247470a895ac1925bbd1de6452a256cdbaaa48d8e3ef7  /usr/bin/pkill
5ba6189beead12a699ffb5e4b1a8fb7ae88f56981e948cb7c7c15776e4f4f63e  /usr/bin/pstree
3318c7fe8256e7357f6ce629dda59b12f95edaa9bb97c1ec4d2d533ba234aace  /usr/bin/runcon
f74e945f696f794d5bb3cc1d9a6b6f32d07061c28a33e14eca6c34248828dec8  /usr/bin/sha1sum
078112c1ed619a2254e8b8d30145eaac1c59f76425176c7b9d4e6a89290c71e3  /usr/bin/sha224sum
08ad66a3d429596f28c674074b6bbedaddb3025a16275ddcd72a7d6058bd5a80  /usr/bin/sha256sum
c12662b9a01357989e9d36c2641da7d3e1b59f9be2818d019ff33284be992f27  /usr/bin/sha384sum
0a075b376d83cc208a7163f6d58b9771e7fb0f8033ca0f0d491712501618f04c  /usr/bin/sha512sum
745c334d493c8a5180316d985efbfd9954426a350a97bbd6d7ba513acf7537de  /usr/bin/size
18c7a3a7d953b0d34a2d83c5f819391d40a45747444b680d8ef46975248bc9b3  /usr/bin/sort
e5cf1101f3223c1487160b82d3174970480049f1cfdcc6243c21b1f984a68ab6  /usr/bin/ssh
dd865aeafccba881d057a1a2e67d6057c79cf0d9433f0d82b316b3b1c1db2680  /usr/bin/stat
c2dca0e0db9d2295b5ca048a1fefa250aa632c84a6be50f6e5521c6161c1cffe  /usr/bin/strace
656ef18961273db4a5c04c70bcae2d6a3b0d7bc2f7b804fb63d891eb78273847  /usr/bin/strings
9c7364b3d17e6aeaf5299b2b1589808ebb01d9a8757fd7d495137eea1e1a438e  /usr/bin/sudo
8f042027dac518e78b06abd491272297d764ef6ce27859c1ed2ca5b438b0428c  /usr/bin/tail
025dfecda71ae62af3204590b869fe05b2d74f66a7773ddbc9ec66520e0fdd9d  /usr/bin/telnet
c941924c1b11acd822c12e4abcefc3d8606b7ae14d989b467f9aadfeeab7c4c4  /usr/bin/test
556870c813935685d5a7e9b89ec93956937037226bbf3732adebad7338795886  /usr/bin/top
ddda14ff84b56f00b1fb781f11d2ffbbc322893a9a2add98014e7d76538c827c  /usr/bin/touch
1f6b1505ed271615822500e1ab469c6be9016b5d80327fb00629be4475718e16  /usr/bin/tr
afcf094cfbf7b2644cb2b8b84800f07d899796860d7d9ffa866d992d28bb4346  /usr/bin/uniq
23b8f998ab52d6b02dc2dfae0ce8e29ee25e4da2309e94d0dfac329e5874fd93  /usr/bin/users
6ecb62ad8bfba3d08a057ff3bbb171051f62e5dae7f0acdab29eb24ba3724847  /usr/bin/vmstat
e10c6009edc0c360c654601cf6d7d0b0daf344ca8ac49504105a297af8be688e  /usr/bin/w
e1b67849062109fd845612d5203709e5b62cd799a180a3be27246d5f24da7d46  /usr/bin/watch
37df1ad316788639d26d1749b7928b0356f2442c180c530075649b1631f7a066  /usr/bin/wc
8b08160118a05cc01ba0a06217ea2266c3acc53fd57b1fe0f7c47d4b84c3a571  /usr/bin/wget
4db12b03ae8a2b9bfdb8d275f71d60b08cf0cc6b92c13062f87960e98d34fc60  /usr/bin/whatis
fc1f9b41b89520db6267dfeb5e4a944e7de8581bea089fba05fa41ebe112b028  /usr/bin/whereis
7bdde142dc5cb004ab82f55adba0c56fc78430a6f6b23afd33be491d4c7c238b  /usr/bin/which
290e2cd3eab2272c2087e91ffabbea5a80817237a12d9740db6842b7ee9c4e0f  /usr/bin/who
12ad4434de2701a3c007e5d1a35e116ee113d279cca06540dca5cd22d4a3c691  /usr/bin/whoami
3a4dc67da3212cea8db8fe0b710649f846ec56b6102424f36074ca87ad716b13  /usr/bin/numfmt
ccb58c1c0a3a080520a3aec958a10303be836107dd1fade6eebc58dc5473bb71  /usr/bin/mawk
019e12ce7fb4adf89ca485ffa54566b8529d824caa8a2644fa89208dae6f78e9  /usr/bin/lwp-request
745c334d493c8a5180316d985efbfd9954426a350a97bbd6d7ba513acf7537de  /usr/bin/x86_64-linux-gnu-size
656ef18961273db4a5c04c70bcae2d6a3b0d7bc2f7b804fb63d891eb78273847  /usr/bin/x86_64-linux-gnu-strings
025dfecda71ae62af3204590b869fe05b2d74f66a7773ddbc9ec66520e0fdd9d  /usr/bin/telnet.netkit
e10c6009edc0c360c654601cf6d7d0b0daf344ca8ac49504105a297af8be688e  /usr/bin/w.procps
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /sbin/depmod
f82307411f0f214daf4ab0a592a1e2eea61381061f9e9080b0652e2e2d66e19c  /sbin/fsck
e0fa4f3ac1b0b0ac2a4e83ff5562962cba15bd24c24dd930597debb2a2052f13  /sbin/ifconfig
ad584ee27af87b3a5d9b8bbdf4d2ce5b1b351e1cd677de0f0e4a41e6624e8843  /sbin/ifdown
ad584ee27af87b3a5d9b8bbdf4d2ce5b1b351e1cd677de0f0e4a41e6624e8843  /sbin/ifup
a3b1b354eb01ad3508f74cda38d00629f0bee0e1280a48d11bc6c89ba4c45166  /sbin/init
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /sbin/insmod
35db9faa40f1329a859f7e37dc90dda3989b70afaaa6e2d4da68bdb57c3777e3  /sbin/ip
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /sbin/lsmod
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /sbin/modinfo
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /sbin/modprobe
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /sbin/rmmod
69140b6ebefbed07900bcb3cc267e1325037e6d38caaa6bc2ae669b3f4ed9c63  /sbin/route
749a81e740b498ec7ea15d00c2a9583d4746e5f90230361c7723567f419ac899  /sbin/runlevel
2048b9a75a5fdf0c7e0334416fde068919d8b2fc60bf1a2c27ae06e92ff62c3f  /sbin/sulogin
02771ef53182cc9d615ef7fdaae1717518a8f832f7c6c25c0a324c80ee911f21  /sbin/sysctl
60e7389ed7264278cbb60eb77d4ddd7023cbfc1184ece1acea8926efe7d5657c  /bin/bash
1c48ade64b96409e6773d2c5c771f3b3c5acec65a15980d8dca6b1efd3f95969  /bin/cat
9cf4cda0f73875032436f7d5c457271f235e59c968c1c101d19fc7bf137e6e37  /bin/chmod
a91d40b349e2bccd3c5fe79664e70649ef0354b9f8bd4658f8c164f194b53d0f  /bin/chown
007d90bd141b2cdbd3cbc5e5f472b3144d122440c73a46d58ee61df2dd71e56e  /bin/cp
1f5e7c13d30a44a211f0cb86e9bf8ff75e7ccf04fed673437b3f86a8893f8bc6  /bin/date
d4ba9ffb5f396a2584fec1ca878930b677196be21aee16ee6093eb9f0a93bf8f  /bin/df
b109cd1771430d88318826916b797d5f4ee87cef05798027856041bac3f1a731  /bin/dmesg
c5ebd611260a9057144fd1d7de48dbefc14e16240895cb896034ae05a94b5750  /bin/echo
a8dd7b4c603616035865afb0c247693e2a43f2fdd7d070aaed43fdd76b804822  /bin/ed
f7c621ae0ceb26a76802743830bc469288996f64342901ae5292950ff713e981  /bin/egrep
5c8b1486de899cdd010d3cacde94579999cb82d0be9ec8c131b1b56886cfd36b  /bin/fgrep
067f7759c089d8ce642bb6c6079463f6575d9aee6dd4c159bda9b3dca76839a4  /bin/fuser
d6edcf90168b9639343c4b246025bbc93f529536c6b39229859ba7bcfe69d08c  /bin/grep
35db9faa40f1329a859f7e37dc90dda3989b70afaaa6e2d4da68bdb57c3777e3  /bin/ip
d703fb83202daf8691aa93ae1a59f2a548520d8e3a6d57a1125d48fbe8ce473c  /bin/kill
d5a7d454ed6305fb65dafb5f552521a051df1cb09dd2cd383a63922857f8715f  /bin/less
ead2dd88155c35412649135b855e875aa048eac36bac000da9e3bce8b67b1cd8  /bin/login
0d06f9724af41b13cdacea133530b9129a48450230feef9632d53d5bbb837c8c  /bin/ls
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /bin/lsmod
173892e4b2a33f2e2ed906ca87cca8bdca91ed8acec6937408b48bf945a42d0c  /bin/mktemp
bf3f5d9c35381206849bd6bf5f9154ed293cf0bb3e56562a834df61ffeb40100  /bin/more
ac504e9f11090a533d34f2e2ef602d3c894605950bf963dbf15bef96a3807e52  /bin/mount
e8ca998df296413624b2bcf92a31ee3b9852f7590f759cc4a8814d3e9046f1eb  /bin/mv
d94a6bbb7c440a9fac2f1fdf98e7895286a145b35d2a6c344951c434f4734173  /bin/netstat
2db30d3fa02acc33c59eaa31e257c5a9d107d546a72c618298f23db4af67e9c9  /bin/ping
f79ad47f8960a7b8b8caae8eb52105375f45e9525f34d64cd5c66b9226abfd0c  /bin/ps
f9f67228b75d850142ad5fffdc58790650a89d2f9bf08938864009a862f97aad  /bin/pwd
38868a265a2852824726c6052f5edbac10399ee732d1799dfb7d9e74bb53c209  /bin/readlink
8d109b799969593c9453226e8a5ee8ba1762bc049a366771fcaa0e2365699c08  /bin/sed
2fdc4546e1f425bcaf62624a7118fd4ba86c11e6277e8b9ee63263eb0dcbc6c5  /bin/sh
c91cad7b606ae8d1e235baee58aa2e2588fc72ab6980fc404ac63af9e713d6dc  /bin/su
ddda14ff84b56f00b1fb781f11d2ffbbc322893a9a2add98014e7d76538c827c  /bin/touch
8946690bfe12308e253054ea658b1552c02b67445763439d1165c512c4bc240d  /bin/uname
7bdde142dc5cb004ab82f55adba0c56fc78430a6f6b23afd33be491d4c7c238b  /bin/which
31e9e2579309d2c68a812d63710cb8257601970bb73344b5ff454d362bde1695  /bin/kmod
a3b1b354eb01ad3508f74cda38d00629f0bee0e1280a48d11bc6c89ba4c45166  /bin/systemd
749a81e740b498ec7ea15d00c2a9583d4746e5f90230361c7723567f419ac899  /bin/systemctl
2fdc4546e1f425bcaf62624a7118fd4ba86c11e6277e8b9ee63263eb0dcbc6c5  /bin/dash
a3b1b354eb01ad3508f74cda38d00629f0bee0e1280a48d11bc6c89ba4c45166  /lib/systemd/systemd"

#debug
#echo "$correctSums"

# convert from readable to csv for later parsing
correctSumsCsv=$(echo "$correctSums"|tr "\n" ","|tr -d "\\" 2>/dev/null)

#debug
#echo "$correctSumsCsv"

fieldLength=$((0+$(echo "$correctSumsCsv" | sed -e 's/[^,]//g'|wc -m)))

#debug
#echo "Field length is $fieldLength"
#echo ""

counter=1

currentBinary="null"

currentSum="null"

# ------ End section ------



# ------ check core binaries ------	# DEBUG REQUIRED
echo "Core binaries check:"
while [ $counter -lt $fieldLength ]; do

	#debug
	#echo "$counter"
	#echo ""

	currentBinary=$(echo $correctSumsCsv|cut -d, -f$counter|cut -d" " -f2)

	#debug
	#echo $currentBinary

	currentSum=$(./sha256sum "$currentBinary")

	#debug
	#echo "$currentSum"

	#debug
	#echo ""

	# extracts SHA256SUM and compares to signature
	if [ "$(echo $currentSum|cut -d' ' -f1)" == "$(echo $correctSumsCsv|cut -d, -f$counter|cut -d" " -f1)" ]; then
		echo -e "$currentBinary\t\t\t\t[ OK ]"|grep --color -i bad
	else
		echo -e "$currentBinary\t\t\t\t[ BAD ]"|grep --color -i bad
	fi

	#debug
	#echo "$(echo $correctSumsCsv|cut -d, -f$counter)"
	#echo "$currentSum"

	counter=$(($counter+1))
done

echo ""
echo ""
# ------ End section ------



# ------ check apt sources ------	# INCOMPLETE
echo "Active APT sources: "
echo ""

# check /etc/apt/sources.list
if [ -e /etc/apt/sources.list ]; then
	echo "/etc/apt/sources.list:"
	echo "$(cat /etc/apt/sources.list|sed -e '/#/d'|egrep -i -- deb\|cd)"
fi

# check /etc/apt/sources.list.d		# INCOMPLETE
if [ -e /etc/apt/sources.list.d/* ]; then
	echo "/etc/apt/sources.list:"
	echo "$(cat /etc/apt/sources.list|sed -e '/#/d'|egrep -i -- deb\|cd)"
fi

echo ""
echo ""
# ------ End section ------



# ------ check $PATH variable ------
echo "\$PATH varible directories:"
echo "$PATH"|tr ':' "\n"
# ------ End section ------
