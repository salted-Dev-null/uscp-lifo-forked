#!/bin/bash

# NOTE TO DEVS:

# Credits:

# Table of contents: (search to jump!)
#	Forensics Question Prompt
#	reset warning
#	pfsearch.sh
#	hardening.sh
#	info.sh
#	UPDATES AND UPGRADES		(GODDMAN YOU!)
#	luserdit.sh


# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



#------ Forensics Question Prompt ------
echo "Did you FINISH ANSWERING forensics questions?!"
read -p "Type UPPERCASE yes to proceed: " proceed

if [ "$proceed" != "YES" ]; then
	echo ""
	echo "------ Warning ------"
	echo "PLEASE COMPLETE FORENSICS QUESTIONS FIRST THEN READ COMPANY POLICIES!"
	echo "------ Warning ------"
	echo ""
	exit 1
else
	echo ""
	echo ""
fi



#------ reset warning ------

# warn user script will reset terminal.

# input cheking step
runInputCheck="true"
while [ "$runInputCheck" == "true" ]; do

	read -p "This script resets terminal and ABOVE TEXT. Continue? (y/n)" run
	if [ -z "$run" -o -z "$(echo $run|egrep -i '^n|^y')" ]; then
		unset run
		echo "Invalid input."
	else
		runInputCheck="false"
	fi
done

# exit if no
if [ -n "$run" -a -n "$(echo $run|egrep -i '^n')" ]; then
	exit
fi

reset

#------ reset warning end -----



#------ pfsearch.sh ------

# prompt user

# input cheking step
runInputCheck="true"
while [ "$runInputCheck" == "true" ]; do

	read -p "Run pfsearch.sh (y/n)?: " run
	if [ -z "$run" -o -z "$(echo $run|egrep -i '^n|^y')" ]; then
		unset run
		echo "Invalid input."
	else
		runInputCheck="false"
	fi
done

# run pfsearch.sh if yes
if [ -n "$run" -a -n "$(echo $run|egrep -i '^y')" ]; then
	./pfsearch.sh &>>pfsearch.log && chmod 666 pfsearch.log &
	echo ""
	echo ""
	echo "Running in Background."
	echo "Wait and check pfsearch.log for command output. (tail -f in new terminal)"
	echo "Press enter to continue."
	read
	reset
fi

#------ pfsearch.sh end ------



#------ hardening.sh ------

# prompt user

# input cheking step
runInputCheck="true"
while [ "$runInputCheck" == "true" ]; do

	read -p "Run hardening.sh (y/n)?: " run
	if [ -z "$run" -o -z "$(echo $run|egrep -i '^n|^y')" ]; then
		unset run
		echo "Invalid input."
	else
		runInputCheck="false"
	fi
done

# run hardening.sh if yes
if [ -n "$run" -a -n "$(echo $run|egrep -i '^y')" ]; then
	./hardening.sh|tee -a hardening.log
	chmod 666 hardening.log
	echo ""
	echo ""
	echo "DONE. Check hardening.log for command output."
	echo "Press enter to continue."
	read
	reset
fi

#------ hardening.sh end ------



#------ info.sh ------

# prompt user

# input cheking step
runInputCheck="true"
while [ "$runInputCheck" == "true" ]; do

	read -p "Run info.sh (y/n)?: " run
	if [ -z "$run" -o -z "$(echo $run|egrep -i '^n|^y')" ]; then
		unset run
		echo "Invalid input."
	else
		runInputCheck="false"
	fi
done

# run info.sh if yes
if [ -n "$run" -a -n "$(echo $run|egrep -i '^y')" ]; then
	./info.sh|tee -a info.log
	chmod 666 info.log
	echo ""
	echo ""
	echo "DONE. Check info.log for command output."
	echo "Press enter to continue."
	read
	reset
fi

#------ info.sh end ------



#------ UPDATES AND UPGRADES ------

# input cheking step
runInputCheck="true"
while [ "$runInputCheck" == "true" ]; do

	read -p "UPGRADE system (y/n)?: " run
	if [ -z "$run" -o -z "$(echo $run|egrep -i '^n|^y')" ]; then
		unset run
		echo "Invalid input."
	else
		runInputCheck="false"
	fi
done

# UPGRADE system if yes
if [ -n "$run" -a -n "$(echo $run|egrep -i '^y')" ]; then
	apt update &>/dev/null &
	apt full-upgrade -y &>/dev/null &
	echo "Upgrading this damn system in background!"
	echo "Please wait till upgrades are done before you un/install software with apt!"
	echo "Press enter to continue."
	read
	reset
fi

#------ UPDATES AND UPGRADES end ------



#------ pacct.sh ------
# COMPEND CODEBLOCK
#------ pacct.sh end ------



#------ luserdit.sh ------

# prompt user

# input cheking step
runInputCheck="true"
while [ "$runInputCheck" == "true" ]; do

	read -p "Run luserdit.sh (y/n)?: " run
	if [ -z "$run" -o -z "$(echo $run|egrep -i '^n|^y')" ]; then
		unset run
		echo "Invalid input."
	else
		runInputCheck="false"
	fi
done

# run luserdit.sh if yes
if [ -n "$run" -a -n "$(echo $run|egrep -i '^y')" ]; then
	./luserdit.sh|tee -a luserdit.log
	chmod 666 luserdit.log
	echo ""
	echo ""
	echo "DONE. Check luserdit.log for command output."
	echo "Press enter to continue."
	read
	reset
fi

#------ luserdit.sh end ------




