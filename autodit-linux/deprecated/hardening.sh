#!/bin/bash

# NOTE TO DEVS:
# GOTTA FISNISH SYSTAT
# JUST FUCKING DO IT! DUMP KALI PENTEST APPS INTO SEARCH PATTERN! 
# JUST FUCKING DO IT! DUMP TORRENT CLIENTS INTO SEARCH PATTERN! 
# SPLIT DEBSUMS AND ETC INTO SYSTEM INTEGRITY CHECK SECTION!
# SPLIT AUDITD, ACCT, SYSTAT, LOGWATCH ETC INTO AUDIT TRAIL SECTION!
# install file integrity tools! get list from lynis!
# SCRIPT IS NOT WORKING ON DEBIAN! ADD PROMPT TO CHANGE OPTIONS!
# DEBIAN CHECK VARIABLE: $isDebian
# DEBUG DEBIAN CHECK IF at LINE 84 (OR WHAT USED TO BE 84)

# Credits:

# Table of contents: (search to jump!)					(CORRECT FORMAT PENDING! END;HEAD;SEC)
#	Distro Check
#	Forensics Question Prompt
#	General Auditing Tips						(COMPLETION PENDING!)
#	Install Prerequisites
#	Preliminary hardening
#		setup firewall
#		install clamav
#		install LMD
#		install Portsentry					(COMPLETION PENDING! INVESTIGATE!)
#		install logwatch					(COMPLETION PENDING! INVESTIGATE!)
#		Legal Banner
#	System integrity tools
#		install debsums
#		install AIDE ?
#		install tripwire
#		install samhain ?
#	Auditing and System info					((PENDING MIGRATION))
#		install auditd
#		install and enable process accounting
#		install and enable sysstat				(COMPEND ENABLE!)
#	Access controls
#		login.defs umask					*1
#		cupsd.conf permissions
#	User Auditing
#		lock passwordless users
#		lock root user
#		disable guest users
#	Password Policies
#		notes
#		password age policy
#			harden access controls				*1
#		pam password complexity and history policy
#		account lockout policy
#	Hacking tools and work only (policy?)
#		remove games
#		list hacking tools					(COMPLETION PENDING!)
#		list torrenting clients					(COMPLETION PENDING!)
#	Advanced hardening
#		SSH hardening
#		Kernel hardening
#		Network hardening
#	Final Notes







# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



#------ Distro Check ------

# debug comment

# variables
#checkInput="true"
#isDebian="false"

#while [ "$checkInput" == "true" ]; do
#	
#	read -p "Is this system running Debian? <debian|any-answer>: " distro
#	
#	# check for debian
#	if [ -n "$distro" -a -n "$(echo \"$distro\"|egrep -E -i '^\(debian\)')"]; then
#		isDebian="true"
#		checkInput="false"
#	elif [ -n "$distro" ]; then
#		isDebian="false"
#		checkInput="false"
#	else
#		echo "Invalid input."
#	fi
#		
#done 

# debug
#echo "$isDebian"

#------ Distro Check End ------



#------ Forensics Question Prompt ------
echo "Did you FINISH ANSWERING forensics questions?!"
read -p "Type UPPERCASE yes to proceed: " proceed

if [ "$proceed" != "YES" ]; then
	echo ""
	echo "------ Warning ------"
	echo "PLEASE COMPLETE FORENSICS QUESTIONS FIRST THEN READ COMPANY POLICIES!"
	echo "------ Warning ------"
	echo ""
	exit 1
else
	echo ""
	echo ""
	echo ""
fi
#------ Forensics Question Prompt End ------



#------ General Auditing Tips ------
echo "------ General Auditing Tips ------"
printf "\nREAD company policies and forensics questions first!!!\n"
printf "General streamlined procedure:\n"
printf "Firewalling --> Install av and ids --> User auditing --> \n"
printf "Configure auto updates --> Install Security Updates --> \n\n"
echo "------ End Section ------"
printf "\n\n\n"
#------ General Auditing Tips End ------



#------ Install Prerequisites ------
echo "------ Install Prerequisites ------"
echo ""

# update software sources
echo "Updating software sources"
apt update 1>/dev/null 2>&1 && echo "DONE" || echo "Something went wrong! We're in terrible terrible denpendency hell!"
echo ""

# install prerequisite software
echo "Installing prerequisite software:"
# debug comment
#apt install ufw clamav clamtk auditd debsums acct libpam-cracklib needrestart needrestart-session -y 1>/dev/null 2>&1 && echo DONE || echo "Prerequisite installation not successful! Please check!"
echo ""

echo "------ End Section ------"
printf "\n\n\n"
#------ Install Prerequisites End ------


#------ Preliminary Hardening ------
echo "------ Preliminary Hardening ------"
echo ""


# setup firewall
echo "Setting up ufw:"
apt install ufw gufw -y 1>/dev/null 2>&1 || echo "G/UFW installation not successful! Please check!"
echo "Allowing ssh acccess..."
ufw allow 22/tcp 1>/dev/null 2>&1 || echo "Something went wrong..."	#allow ssh
echo "Reject all incoming..."
ufw default reject incoming 1>/dev/null 2>&1 || echo "Something went wrong..."
echo "Setup logging (MEDIUM)..."
ufw logging on 1>/dev/null 2>&1 || echo "Something went wrong..."
ufw logging medium 1>/dev/null 2>&1
echo "Enabling firewall..."
ufw enable || echo "Something went wrong. Please check firewall configuration!"
echo ""


# install clamav
echo "Installing clamav: (don't forget to configure auto scan in clamav!)"
apt install clamav clamtk -y 1>/dev/null 2>&1 && successBit="1" || echo "clamav installation not successful! Please check!"

if [ "$successBit" == "1" ]; then
	echo "clamav and clamtk installed."
fi
successBit="0"

echo ""


# install LMD
# define variables here!
correctSum="ac6a2d0ba371c6be7a42d3fa49bfe0a24b8162aaef1ed4ea9ecc121e40442fbf  maldetect-current.tar.gz" #sha256 digest

echo "Installing LMD:"

# download and integrity check
wget http://www.rfxn.com/downloads/maldetect-current.tar.gz 1>/dev/null 2>&1
sha256sum maldetect-current.tar.gz>dlSum
echo "$correctSum" > ./corSum
result=$(diff corSum dlSum)

#debug
#echo "$result"

if [ -n "$result" ]; then
	echo "Download sha256 sum is incorrect! exiting LMD installation"
elif [ -z "$result" ]; then
	echo "Download sha256 sum is correct! proceeding with LMD installation"

	gunzip maldetect-current.tar.gz
	tar -xf maldetect-current.tar
	cd maldetect-1.6.3/
	bash ./install.sh 1>/dev/null 2>&1 && successBit="1" && echo "LMD is installed." || echo "Something went wrong installing LMD! Please check!"
	cd ../

fi

rm -r mal*
rm corSum dlSum

echo ""


# Legal Banner
echo "Editing legal banner!"
cp -a /etc/issue /etc/issue_bak
cp -a /etc/issue.net /etc/issue.net_bak

# banner variable
bannerText="\n\
|-----------------------------------------------|\n\
|WARNING: THIS COMPUTER IS PROPERTY OF TEAM LIFO|\n\
|    THIS COMPUTER IS FOR BUSINESS USE ONLY!    |\n\
|          YOUR ACTIONS ARE MONITORED!          |\n\
|-----------------------------------------------|\n\
"
# deduplication canary for /etc/issue
if [ -z "$(egrep -E '^[|][-]*[|]' /etc/issue)" ]; then
	echo "$banner" >> /etc/issue
fi
# deduplication canary for /etc/issue.net
if [ -z "$(egrep -E '^[|][-]*[|]' /etc/issue.net)" ]; then
	echo "$banner" >> /etc/issue.net
fi


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Preliminary Hardening End ------



#------ System Integrity Tools ------
echo "------ System Integrity Tools ------"
echo ""


# install debsums
echo "Installing debsums:"
apt install debsums -y 1>/dev/null 2>&1 && echo DONE || echo "debsums installation not successful! Please check!"
echo""


# install tripwire
echo "Installing tripwire:"
echo "Checksum database file is encrypted with -P option!"
echo "Please enter password when initializing!"
echo "run command <tripwire --init -P> after system is threat free! "
echo "Press enter to continue: "
read
apt install tripwire -y 2>/dev/null && echo DONE || echo "tripwire installation not successful! Please check!"
echo ""





echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ System Integrity Tools End ------



#------ Auditing and System info ------
echo "------ Auditing and System info ------"
echo ""


# install auditd
echo "Installing auditd:"
apt install auditd -y 1>/dev/null 2>&1 && successBit="1" || echo "auditd installation not successful! Please check!"
if [ "$successBit" == "1" ]; then
	echo "Enabling auditd: (Edit /etc/audit/auditd.conf and lock audting till next reboot using auditctl -e 2)"
	echo ""

	echo "auditd status:"
	auditctl -e 1

fi
successBit="0"
echo ""


# install and enable process accounting
echo "Installing process accounting: (use pacct.sh for further auditing)"

# install acct
apt install acct -y 1>/dev/null 2>&1 && successBit="1" || echo "acct installation not successful! Please check!"

# enable acct
if [ "$successBit" == "1" ]; then
	touch /var/log/pacct
	chmod 750 /var/log/pacct
	accton /var/log/pacct && echo "DONE" || "Something went wrong enabling acct! Please check!"
fi
successBit="0"
echo ""


# install and enable sysstat
echo "Installing sysstat:"
apt install sysstat -y 1>/dev/null 2>&1 && echo "DONE" && successBit="1" || echo "Something went wrong installing systat! Please check!"

# enable sysstat
if [ "$successBit" == "1" ]; then
	#debug
	echo "debug codeblock..."
fi
successBit="0"


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Auditing and System info End ------



#------ Access Controls ------
echo "------ Access Controls ------"
echo ""


# login.defs umask
echo "login.defs umask hardening is included in password policy. See section \"password age policy - harden access controls\". "
echo ""


# cupsd.conf permissions
echo "Changing cupsd.conf file permissions:"
chmod o-rwx /etc/cups/cupsd.conf && echo "DONE" || "cupsd isn't installed or something went wrong..."
echo ""


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Access Controls End------



#------  User Auditing  ------
echo "------ User Auditing ------"
echo ""


# declare variables here!!!
listFields=0
currentUser=null


# get passwordless users
npUsers=$(passwd -Sa| grep " NP " |cut -f1 -d" "|tr "\n" ",")
listFields=$(echo $npUsers |grep -o ","|wc -l)
currentUser=null


# lock passwordless users
echo "Locking passwordless users: "

for i in `seq 1 $listFields`
do
	currentUser=$(echo "$npUsers"| cut -f$i -d",")
	passwd -l "$currentUser" 1>/dev/null 2>&1 || echo "Something went wrong locking $currentUser!"
done
echo "DONE"
echo ""


# lock root user
echo "Locking down root: (local login and remote)"
passwd -l root 1>/dev/null 2>&1
cp -na /etc/ssh/sshd_config /etc/ssh/sshd_config_bak 1>/dev/null 2>&1

if [ -e /etc/ssh/sshd_config -a "$(head -n1 /etc/ssh/sshd_config)" != "# autodit was here..." ]; then
	sed -i /etc/ssh/sshd_config -e "s/PermitRootLogin.*/PermitRootLogin=no/g" 1>/dev/null 2>&1
	# canary bit
	sed -i /etc/ssh/sshd_config -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug sshd_config" #debug
fi

echo "DONE"
echo ""


# disable guest users
echo "Disabling guest account:"
disableGuest=0

if [ -e /etc/lightdm/lightdm.conf ]; then
	echo "Detected lightdm..."
	cp -na /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf_bak

	# deduplication canary
	if [ "$(head -n1 /etc/lightdm/lightdm.conf)" != "# autodit was here..." ]; then
		echo "allow-guest=false">>/etc/lightdm/lightdm.conf
		# canary bit
		sed -i /etc/lightdm/lightdm.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug lightdm" #debug
	fi

	disableGuest=1
	echo "Manually restart lightdm later! (sudo systemctl restart lightdm.service)"
fi

# GDM INVESTIGATION PENDING!

if [ "$disableGuest" == 1 ]; then
	echo "Guest account disabled."
else
	echo "Lightdm not detected, please manually disable guest if applicable!"
fi


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ User Auditing End ------



#------ Password Policies ------
echo "------ Password Policies ------"
echo ""


# notes
echo "Notes: use chpasswd to batch change passwords"
echo ""


# password age policy
echo "Changing password age policies in login.defs:"
cp -na /etc/login.defs /etc/login.defs_bak

# deduplication canary
if [ "$(head -n1 /etc/login.defs)" != "# autodit was here..." ]; then
	sed -e "s/PASS_MIN_DAYS.*/PASS_MIN_DAYS\ 0/g" -i /etc/login.defs
	sed -e "s/PASS_MAX_DAYS.*/PASS_MAX_DAYS\ 90/g" -i /etc/login.defs
	sed -e "s/PASS_WARN_AGE.*/PASS_WARN_AGE\ 14/g" -i /etc/login.defs

	# harden access controls
	sed -i /etc/login.defs -r -e 's/^(UMASK)([^0-9]*)([0-9]*)/\1\2027/gi'

	# canary bit
	sed -i /etc/login.defs -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug login.defs" #debug
fi

echo "Done."
echo ""


# pam password complexity and history policy
echo "Configuring PAM.d password and auth policies:"
apt install libpam-cracklib -y 1>/dev/null 2>&1
cp -na /etc/pam.d/common-password /etc/pam.d/common-password_bak

# deduplication canary
if [ "$(head -n1 /etc/pam.d/common-password)" != "# autodit was here..." ]; then
	sed -e 's/\(pam_unix\.so.*\)/\1 remember=5/g' -i /etc/pam.d/common-password
	sed -e "s/\(pam_cracklib\.so.*\)/\1 ucredit=-1\ lcredit=-1\ dcredit=-1\ ocredit=-1\ enforce_for_root/g" -i /etc/pam.d/common-password
	
	# check if minlen exists and if its value is above 8
	checkString="$(cat /etc/pam.d/common-password|grep -i pam_cracklib|grep -i minlen)"
	checkLength=$(echo "$checkString"|sed -r -e 's/.*minlen=([0-9]*).*/\1/')
	if [ -z "$checkString" ]; then
		sed -r -e "s/(.*pam_cracklib\.so.*)/\1 minlen=8" -i /etc/pam.d/common-password
	elif [ "$checkLength" -lt "8" ]; then
		sed -r -e 's/(.*)(minlen=[0-9]*)(.*)/\1minlen=8\3/' -i /etc/pam.d/common-password
	fi
	
	# canary bit
	sed -i /etc/pam.d/common-password -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug common-password" #debug
fi

echo "Done"
echo ""


# account lockout policy
echo "Configuring account lockout policies:"
cp -na /etc/pam.d/common-auth /etc/pam.d/common-auth_bak

# deduplication canary
if [ "$(head -n1 /etc/pam.d/common-auth)" != "# autodit was here..." ]; then

	# insert tally2 rule before deny
	sed -i /etc/pam.d/common-auth -e 's/\(.*pam_deny.so.*\)/auth\trequired\tpam_tally2.so deny=5 onerr=fail unlock_time=1800\n\1/g'
	
	# increase line skip on success to accommodate above rule
	successSkip=$(grep -i "success" /etc/pam.d/common-auth|sed -r -e "/#/d" -e "s/(.*success=)([0-9]*)(.*)/\2/")
	successSkip=$((successSkip+1))
	sed -r -i /etc/pam.d/common-auth -e "s/success=[0-9]*/success=$successSkip/"
	
	# canary bit
	sed -i /etc/pam.d/common-auth -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug common-auth" #debug
fi

echo "DONE"
echo ""


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Password Policies End ------



#------ Hacking tools and work only (policy?) ------		(WORDING AND COMPLETION PENDING!)
echo "------ Hacking tools and work only (policy?) ------"
echo ""


# remove games						(SEARCH AND DESTROY PENDING!)
echo "Removing games:"
apt-get purge gnome-2048 aisleriot atomix gnome-chess five-or-more hitori iagno gnome-klotski lightsoff \
gnome-mahjongg gnome-mines gnome-nibbles quadrapassel four-in-a-row gnome-robots gnome-sudoku swell-foop \
tali gnome-taquin gnome-tetravex -y 1>/dev/null 2>&1
apt autoremove -y 1>/dev/null 2>&1
echo "DONE, check for more if not completely removed"
echo ""


# list hacking tools						(COMPLETION PENDING DAMMNIT!)
echo "Hacking tools (installed, not background malware): (audit command: sudo apt purge <hacking tool> -y) (DONT LIST ON DOCUMENTATION)"
#debug
echo "Debug: codeblock ..."
echo ""


# list torrenting clients					(COMPLETION PENDING DAMMNIT!)
echo "Torrenting clients: (audit command: sudo apt purge <torrenting client> -y) (DONT LIST ON DOCUMENTATION)"
#debug
echo "Debug: codeblock ..."
echo ""


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Hacking tools and work only (policy?) End ------



#------ Advanced hardening ------
echo "------ Advanced hardening ------"
echo ""


# SSH hardening
echo "Hardening SSH configuration: (pubkey authentication not configured)"

# deduplication canary
if [ -e /etc/ssh/sshd_config -a "$(tail -n1 /etc/ssh/sshd_config)" != "# autodit-ssh was here..." ]; then
	cp -na /etc/ssh/sshd_config /etc/ssh/sshd_config_bak

	echo "" >> /etc/ssh/sshd_config
	echo "" >> /etc/ssh/sshd_config
	echo "# autodit custom config" >> /etc/ssh/sshd_config

	# comment out relevant settings
	sed 's/^\(AllowTcpForwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(ClientAliveCountMax\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(ClientAliveInterval\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Compression\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(GatewayPorts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(IgnoreRhosts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(LoginGraceTime\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(LogLevel\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MaxAuthTries\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MaxSessions\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitRootLogin\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitUserEnvironment\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitTunnel\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PrintLastLog\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(StrictModes\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(TCPKeepAlive\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(UseDNS\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(X11Forwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(AllowAgentForwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitEmptyPasswords\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PubkeyAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PasswordAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Protocol\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(HashKnownHosts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Banner\)/\#\1/gI' -i /etc/ssh/sshd_config

	# change settings through append
	echo "AllowTcpForwarding no" >> /etc/ssh/sshd_config
	echo "ClientAliveCountMax 2" >> /etc/ssh/sshd_config
	echo "ClientAliveInterval 300" >> /etc/ssh/sshd_config
	echo "Compression no" >> /etc/ssh/sshd_config
	echo "GatewayPorts no" >> /etc/ssh/sshd_config
	echo "IgnoreRhosts yes" >> /etc/ssh/sshd_config
	echo "LoginGraceTime 1m" >> /etc/ssh/sshd_config
	echo "LogLevel VERBOSE" >> /etc/ssh/sshd_config
	echo "MaxAuthTries 2" >> /etc/ssh/sshd_config
	echo "MaxSessions 2" >> /etc/ssh/sshd_config
	echo "PermitRootLogin no" >> /etc/ssh/sshd_config
	echo "PermitUserEnvironment no" >> /etc/ssh/sshd_config
	echo "PermitTunnel no" >> /etc/ssh/sshd_config
	echo "PrintLastLog yes" >> /etc/ssh/sshd_config
	echo "StrictModes yes" >> /etc/ssh/sshd_config
	echo "TCPKeepAlive no" >> /etc/ssh/sshd_config
	echo "UseDNS no" >> /etc/ssh/sshd_config
	echo "X11Forwarding no" >> /etc/ssh/sshd_config
	echo "AllowAgentForwarding no" >> /etc/ssh/sshd_config
	echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config
#	echo "PubkeyAuthentication\ yes" >> /etc/ssh/sshd_config		# uncomment if use pubkey
#	echo "PasswordAuthentication\ no" >> /etc/ssh/sshd_config		# uncomment if use pubkey
	echo "Protocol 2" >> /etc/ssh/sshd_config
#	echo "HashKnownHosts yes" >> /etc/ssh/sshd_config
	echo "Banner /etc/issue.net" >> /etc/ssh/sshd_config

	# canary bit
	echo "# autodit-ssh was here..." >> /etc/ssh/sshd_config # && echo "debug autodit-ssh" # debug

	systemctl restart sshd.service && echo "DONE"|| echo "Something went wrong with SSH. Check /etc/ssh/sshd_config!"

fi
echo ""



# Kernel hardening
echo "Hardening kernel: "

cp -na /etc/sysctl.conf /etc/sysctl.conf_bak

# deduplication canary
if [ -e /etc/sysctl.conf -a "$(head -n1 /etc/sysctl.conf)" != "# autodit was here..." ]; then

	# comment out relevant settings

	# lynis configurations
	sed -e 's/^\(fs\.protected_hardlinks\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(fs\.protected_symlinks\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(fs\.suid_dumpable\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.core_uses_pid\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.ctrl-alt-del\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.dmesg_restrict\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(kernel\.kptr_restrict\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.randomize_va_space\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.sysrq\)/\#\1/gI' -i /etc/sysctl.conf
#	sed -e 's/^\(kernel\.yama.ptrace_scope\)/\#\1/gI' -i /etc/sysctl.conf	#BREAKS VMWARE! REEEE!
	sed -e 's/^\(net\.ipv4.conf\.all\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.conf\.all\.bootp_relay\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.log_martians\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.mc_forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.proxy_arp\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.rp_filter\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.conf\.all\.send_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.log_martians\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.icmp_echo_ignore_broadcasts\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.icmp_ignore_bogus_error_responses\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.tcp_syncookies\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.tcp_timestamps\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	#lynis configurations end

	sed -e 's/^\(net\.ipv4\.ip_forward\)/\#\1/gI' -i /etc/sysctl.conf
	#disable ipv6	(this part is optional)
	sed -e 's/^\(net\.ipv6\.conf\.all\.disable_ipv6\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.disable_ipv6\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.lo\.disable_ipv6\)/\#\1/gI' -i /etc/sysctl.conf

	# change settings through append
	echo "" >> /etc/sysctl.conf
	echo "" >> /etc/sysctl.conf
	echo "# autodit kernel hardening configurations:" >> /etc/sysctl.conf

	# lynis configurations
	echo "fs.protected_hardlinks = 1" >> /etc/sysctl.conf
	echo "fs.protected_symlinks = 1" >> /etc/sysctl.conf
	echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf
	echo "kernel.core_uses_pid = 1" >> /etc/sysctl.conf
	echo "kernel.ctrl-alt-del = 0" >> /etc/sysctl.conf
	echo "kernel.dmesg_restrict = 1" >> /etc/sysctl.conf

	echo "kernel.kptr_restrict = 2" >> /etc/sysctl.conf
	echo "kernel.randomize_va_space = 2" >> /etc/sysctl.conf
	echo "kernel.sysrq = 0" >> /etc/sysctl.conf
#	echo "kernel.yama.ptrace_scope = 2" >> /etc/sysctl.conf	#BREAKS VMWARE! REEEE!
	echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.accept_source_route = 0" >> /etc/sysctl.conf

	echo "net.ipv4.conf.all.bootp_relay = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.log_martians = 1" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.mc_forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.proxy_arp = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.rp_filter = 1" >> /etc/sysctl.conf

	echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.log_martians = 1" >> /etc/sysctl.conf
	echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" >> /etc/sysctl.conf
	echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" >> /etc/sysctl.conf

	echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_timestamps = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	# lynis configuration end

	echo "net.ipv4.ip_forward = 0" >> /etc/sysctl.conf
	#disable ipv6	(optional)
	echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf

	# reload sysctl.conf
	sysctl -p

	# deduplication canary
	sed -i /etc/sysctl.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' && echo "DONE" # && echo "debug kernel-harden" #debug

else
	echo "Kernel was hardened by autodit. Append check additional configurations manually!"

fi

echo ""


# Network hardening
echo "Hardneing network settings: "

cp -na /etc/host.conf /etc/host.conf_bak

# deduplication canary
if [ -e /etc/host.conf -a "$(head -n1 /etc/host.conf)" != "# autodit was here..." ]; then

	echo "nospoof on" >> /etc/host.conf
	# deduplication canary
	sed -i /etc/host.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' && echo "DONE" # && echo "debug host.conf" #debug

else
	echo "Network settings were hardened by autodit. Append check additional configurations manually!"

fi


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Advanced hardening End ------



#------ Final Notes ------
echo "------ Final Notes ------"
echo ""
echo "Don't forget to:"
echo "1: Turn on auto updates in GUI"
echo "2: Turn on auto security updates in GUI"
echo "3: Get latest version of lynis, chkrootkit and rkhunter then SCAN (including clamav/tk)" 
echo "4: Update and upgrade system! (Takes a buttload of time.)"
echo "5: GLHF! SKIP IF PAST 20MIN TIME!"
echo ""
echo "------ Final Notes End ------"
#------ Final Notes End ------
