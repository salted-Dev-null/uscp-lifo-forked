#!/bin/bash

# DOCUMENTATION PENDING



# DE-ADMIN SCRIPT



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



# define variables here!
currentUser=""
deadminList=$1
fieldLength=""



# test input and get Field Length
if [ -z "$deadminList" ]; then
	echo "Please enter comma seperated users!"
	exit 1
else
	fieldLength=$((0+$(echo "$deadminList" | sed -e 's/[^,]//g'|wc -m)))
	if [ -z "$(echo $deadminList|cut -d\, -f $(($fieldLength)))" ]; then
		fieldLength=$(($fieldLength-1))
	fi
fi



# de-escalate admins
for i in `seq 1 $fieldLength`; do
	currentUser="$(echo $deadminList|cut -f $i -d',')"

	if [ -n "$currentUser" ]; then
		deluser $currentUser sudo
		deluser $currentUser adm
		deluser $currentUser wheel
		deluser $currentUser root
		deluser $currentUser lpadmin
	fi

done
