#!/bin/bash

# NOTE TO DEVS:
# TEST IN VM! WILL BREAK MACHINE FAST!!!!!
# DOCUMENTATION PENDING!
# PENDING DEBUG!
# REFER TO lusas, harrisburg-linux.sh and lynis
# CREATE process accounting scripts! refer to (https://www.tldp.org/HOWTO/text/Process-Accounting) and lynis
# update and upgrade first thing then make todo list!
# seperate file-search to seperate script
# streamline: forensics questions-->filesearch-->autodit-->update&upgrade


# Credits:
#	Script created by Jaeger105 for team LIFO competing Cyberpatriot-XI.
#	Script mostly written by Jaeger105. Referred to harrisburg-linux 
#	written by JoshuaTatum 
#	(https://github.com/JoshuaTatum/cyberpatriot), lusas written by 
#	Boran (https://github.com/Boran/lusas) and lynis by CISOfy 
#	(https://github.com/Boran/lusas). Please credit author and sources 
#	used in the script when you use it or modify it, thank you!
#	I'd also like to thank Mr. Wong for giving us this opportunity to
#	compete in CP-XI, Light for and Migzace for looking over the code
#	and debugging!



# Table of contents: (search to jump!)					(CORRECT FORMAT PENDING! END;HEAD;SEC)
#	Forensics Question Prompt
#	General Auditing Tips						(COMPLETION PENDING!)
#	Preliminary hardening
#		setup firewall
#		install clamav
#		install LMD
#		install auditd
#		install debsums
#		install and enable process accounting
#		Portsentry and logwatch					(COMPLETION PENDING! INVESTIGATE!)
#		.bashrc and which all check				(COMP PEND! REFER TO NDG LINUX!)
#	User Auditing
#		get users with UID 0
#		get active users, filter results and display privileges
#		list users with admin privileges
#		lock passwordless users
#		lock root user
#		disable guest users
#	Password Policies
#		notes
#		password age policy					(umask 027 PENDING! INVESTIGATE!)
#		pam password complexity and history policy
#		account lockout policy
#	List open ports and established connections
#		list open TCP ports
#		list established connections
#		list open UDP connections
#		list network services
#	Hacking tools and work only (policy?)
#		remove games
#		list hacking tools					(COMPLETION PENDING!)
#		list torrenting clients					(COMPLETION PENDING!)
#		prohibited files: (file search pattern: harrisburg-linux and wannacrypt)	(DEBUG PENDING!)
#	Advanced hardening
#		SSH hardening
#		Kernel hardening					(ADD OPTIONAL DISABLE IPV6)
#		Network hardening



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



#------ Forensics Question Prompt ------
echo "Did you FINISH ANSWERING forensics questions?!"
read -p "Type UPPERCASE yes to proceed: " proceed

if [ "$proceed" != "YES" ]; then
	echo ""
	echo "------ Warning ------"
	echo "PLEASE COMPLETE FORENSICS QUESTIONS FIRST THEN READ COMPANY POLICIES!"
	echo "------ Warning ------"
	echo ""
	exit 1
else
	echo ""
	echo ""
	echo ""
fi


#------ General Auditing Tips ------
echo "------ General Auditing Tips ------"
printf "\nREAD company policies and forensics questions first!!!\n"
printf "General streamlined procedure:\n"
printf "Firewalling --> Install av and ids --> User auditing --> \n"
printf "Configure auto updates --> Install Security Updates --> \n\n"
echo "------ End Section ------"
printf "\n\n\n"



# update software sources
echo "Updating software sources"
apt update 1>/dev/null 2>&1 && echo debug-update #debug
echo ""
echo ""
echo ""



#------ Preliminary Hardening ------
echo "------ Preliminary Hardening ------"
echo ""


# setup firewall
echo "Setting up ufw:"
apt install ufw -y 1>/dev/null 2>&1 || echo "G/UFW installation not successful! Please check!"
echo "Allowing ssh acccess..."
ufw allow 22/tcp 1>/dev/null 2>&1 || echo "Something went wrong..."	#allow ssh
echo "Reject all incoming..."
ufw default reject incoming 1>/dev/null 2>&1 || echo "Something went wrong..."
echo "Setup logging (MEDIUM)..."
ufw logging on 1>/dev/null 2>&1 || echo "Something went wrong..."
ufw logging medium 1>/dev/null 2>&1
echo "Enabling firewall..."
ufw enable || echo "Something went wrong. Please check firewall configuration!"
echo ""

echo "UFW status:"
ufw status verbose 2>/dev/null  || echo "Something went wrong..."

echo ""


# install clamav
echo "Installing clamav: (don't forget to configure auto scan in clamav!)"
apt install clamav clamtk -y 1>/dev/null 2>&1 && successBit="1" || echo "clamav installation not successful! Please check!"

if [ "$successBit" == "1" ]; then
	echo "clamav and clamtk installed."
fi
successBit="0"

echo ""


# install LMD
# define variables here!
correctSum="ac6a2d0ba371c6be7a42d3fa49bfe0a24b8162aaef1ed4ea9ecc121e40442fbf  maldetect-current.tar.gz" #sha256 digest

echo "Installing LMD:"

# download and integrity check
wget http://www.rfxn.com/downloads/maldetect-current.tar.gz 1>/dev/null 2>&1
sha256sum maldetect-current.tar.gz>dlSum
echo "$correctSum" > ./corSum
result=$(diff corSum dlSum)

#debug
#echo "$result"

if [ -n "$result" ]; then
	echo "Download sha256 sum is incorrect! exiting LMD installation"
elif [ -z "$result" ]; then
	echo "Download sha256 sum is correct! proceeding with LMD installation"

	gunzip maldetect-current.tar.gz
	tar -xf maldetect-current.tar
	cd maldetect-1.6.3/
	bash ./install.sh 1>/dev/null 2>&1 && successBit="1" && echo "LMD is installed." || echo "Something went wrong installing LMD! Please check!"
	cd ../

fi

rm -r mal*
rm corSum dlSum

echo ""


# install auditd
echo "Installing auditd:"
apt install auditd -y 1>/dev/null 2>&1 && successBit="1" || echo "auditd installation not successful! Please check!"
if [ "$successBit" == "1" ]; then
	echo "Enabling auditd: (Edit /etc/audit/auditd.conf and lock audting till next reboot using auditctl -e 2)"
	echo ""

	echo "auditd status:"
	auditctl -e 1

fi
successBit="0"

echo ""

# install debsums
echo "Installing debsums:"
apt install debsums -y 1>/dev/null 2>&1 && successBit="1" || echo "debsums installation not successful! Please check!"
if [ "$successBit" == "1" ]; then
	echo "DONE"
	echo ""
fi
successBit="0"



# install and enable process accounting
echo "Installing process accounting: (use pacct.sh for further auditing)"

# install acct
apt install acct -y 1>/dev/null 2>&1 && successBit="1" || echo "acct installation not successful! Please check!"

# enable acct
if [ "$successBit" == "1" ]; then
	touch /var/log/pacct
	chmod 750 /var/log/pacct
	accton /var/log/pacct && echo "DONE" || "Something went wrong enabling acct! Please check!"
fi
successBit="0"


echo ""
echo "------ End Section ------"
printf "\n\n\n"



#------  User Auditing  ------
echo "------ User Auditing ------"
echo ""


# get users with UID 0
echo "Users with UID 0:"
cut -f1,3 -d":" /etc/passwd|grep ":0"
echo ""


# get active users, filter results and display privileges
# declare variables here!!!
activeUsers=null
listFields=0
currentUser=null

echo "Active Users on server: (command to audit: userdel -r <USER>)"

activeUsers=$(cat /etc/passwd|sed -e "/nologin/d"|sed -e "/false/d" -e "/sync/d"|cut -f1 -d":"|tr "\n" ",")

listFields=$(echo $activeUsers |grep -o ","|wc -l)

for i in `seq 1 $listFields`
do
	groups $(echo "$activeUsers" |cut -f$i -d",")
done
echo ""


# list users with admin privileges
echo "Users with admin privileges: (command to audit: deluser <user> <admin group>)"

for i in `seq 1 $listFields`
do
        currentUser=$(groups $(echo "$activeUsers" |cut -f$i -d","))
	echo "$currentUser"|grep "admin\|adm\|sudo\|wheel\|root"|cut -f1 -d " "

done
echo ""


# get passwordless users
npUsers=$(passwd -Sa| grep " NP " |cut -f1 -d" "|tr "\n" ",")
listFields=$(echo $npUsers |grep -o ","|wc -l)
currentUser=null

echo "Users with no passwords on server: (Command to audit: usermod -l)"

for i in `seq 1 $listFields`
do
	groups $(echo "$npUsers" |cut -f$i -d",")
done
echo ""


# lock passwordless users
echo "Locking passwordless users: "

for i in `seq 1 $listFields`
do
	currentUser=$(echo "$npUsers"| cut -f$i -d",")
	passwd -l "$currentUser" 1>/dev/null 2>&1 || echo "Something went wrong locking $currentUser!"
done
echo "DONE"
echo ""


# lock root user
echo "Locking down root: (local login and remote)"
passwd -l root 1>/dev/null 2>&1
cp -na /etc/ssh/sshd_config /etc/ssh/sshd_config_bak 1>/dev/null 2>&1

if [ -e /etc/ssh/sshd_config -a "$(head -n1 /etc/ssh/sshd_config)" != "# autodit was here..." ]; then
	sed -i /etc/ssh/sshd_config -e "s/PermitRootLogin.*/PermitRootLogin=no/g" 1>/dev/null 2>&1
	# canary bit
	sed -i /etc/ssh/sshd_config -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug sshd_config" #debug
fi

echo "DONE"
echo ""


# disable guest users
echo "Disabling guest account:"
disableGuest=0

if [ -e /etc/lightdm/lightdm.conf ]; then
	echo "Detected lightdm..."
	cp -na /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf_bak

	# deduplication canary
	if [ "$(head -n1 /etc/lightdm/lightdm.conf)" != "# autodit was here..." ]; then
		echo "allow-guest=false">>/etc/lightdm/lightdm.conf
		# canary bit
		sed -i /etc/lightdm/lightdm.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug lightdm" #debug
	fi

	disableGuest=1
	echo "Manually restart lightdm later! (sudo systemctl restart lightdm.service)"
fi

# GDM INVESTIGATION PENDING!

if [ "$disableGuest" == 1 ]; then
	echo "Guest account disabled."
else
	echo "Lightdm not detected, please manually disable guest if applicable!"
fi


echo ""
echo "------ End Section ------"
printf "\n\n\n"



#------ Password Policies ------
echo "------ Password Policies ------"
echo ""


# notes
echo "Notes: use chpasswd to batch change passwords"
echo ""


# password age policy
echo "Changing password age policies in login.defs:"
cp -na /etc/login.defs /etc/login.defs_bak

# deduplication canary
if [ "$(head -n1 /etc/login.defs)" != "# autodit was here..." ]; then
	sed -e "s/PASS_MIN_DAYS.*/PASS_MIN_DAYS\ 7/g" -i /etc/login.defs
	sed -e "s/PASS_MAX_DAYS.*/PASS_MAX_DAYS\ 90/g" -i /etc/login.defs
	sed -e "s/PASS_WARN_AGE.*/PASS_WARN_DAYS\ 14/g" -i /etc/login.defs

	# canary bit
	sed -i /etc/login.defs -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug login.defs" #debug

	# harden access controls
	umask 027 

fi

echo "Done."
echo ""


# pam password complexity and history policy
echo "Configuring PAM.d password and auth policies:"
apt install libpam-cracklib -y 1>/dev/null 2>&1
cp -na /etc/pam.d/common-password /etc/pam.d/common-password_bak

# deduplication canary
if [ "$(head -n1 /etc/pam.d/common-password)" != "# autodit was here..." ]; then
	sed -e 's/\(pam_unix\.so.*\)/\1 remember=5\ minlen=8/g' -i /etc/pam.d/common-password
	sed -e "s/\(pam_cracklib\.so.*\)/\1 ucredit=-1\ lcredit=-1\ dcredit=-1\ ocredit=-/" -i /etc/pam.d/common-password
	# canary bit
	sed -i /etc/pam.d/common-password -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug common-password" #debug
fi

echo "Done"
echo ""


# account lockout policy
echo "Configuring account lockout policies:"
cp -na /etc/pam.d/common-auth /etc/pam.d/common-auth_bak

# deduplication canary
if [ "$(head -n1 /etc/pam.d/common-auth)" != "# autodit was here..." ]; then
	echo "auth	required	pam_tally2.so deny=5 onerr=fail unlock_time=1800" >> /etc/pam.d/common-auth
	# canary bit
	sed -i /etc/pam.d/common-auth -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug common-auth" #debug
fi

echo "DONE"
echo ""


echo ""
echo "------ End Section ------"
printf "\n\n\n"



#------ List open ports and established connections ------
echo "------ Open ports and listening services ------"
echo ""


# notes
echo "notes: when in doubt, GOOGLE!"
echo "command to audit: apt purge <non-critical services> -y"
echo ""


# list open TCP ports
echo "Open ports (behind firewall): "
netstat -antpul|grep -i listen
printf "\n"


# list established connections
echo "Established and other connections: "
netstat -antpul|grep -i 'tcp'|sed -e "/LISTEN/d"
printf "\n"


# list UDP connections
echo "UDP connections: "
netstat -antpul|grep -i "udp"
echo""


# list network services
echo "Possible networked services: (refers above)"
#debug comment-out
#netstat -antpulW|tail -n +3|sed 's/.*\///g'|sort|uniq
networkedServices="$(netstat -antpulW)"
parsedServices="$(echo "$networkedServices"|tail -n +3|grep '/'|sed 's/.*\///g'|sort|uniq)"
echo "$parsedServices"


echo ""
echo "------ End Section ------"
printf "\n\n\n"



#------ Hacking tools and work only (policy?) ------		(WORDING AND COMPLETION PENDING!)
echo "------ Hacking tools and work only (policy?) ------"
echo ""


# remove games						(SEARCH AND DESTROY PENDING!)
echo "Removing games:"
apt-get purge gnome-2048 aisleriot atomix gnome-chess five-or-more hitori iagno gnome-klotski lightsoff \
gnome-mahjongg gnome-mines gnome-nibbles quadrapassel four-in-a-row gnome-robots gnome-sudoku swell-foop \
tali gnome-taquin gnome-tetravex bsdgames* -y 1>/dev/null 2>&1
apt autoremove -y 1>/dev/null 2>&1
echo "DONE, check for more if not completely removed"
echo ""


# list hacking tools						(COMPLETION PENDING DAMMNIT!)
echo "Hacking tools (installed, not background malware): (audit command: sudo apt purge <hacking tool> -y) (DONT LIST ON DOCUMENTATION)"
#debug
echo "Debug: codeblock ..."
echo ""


# list torrenting clients					(COMPLETION PENDING DAMMNIT!)
echo "Torrenting clients: (audit command: sudo apt purge <torrenting client> -y) (DONT LIST ON DOCUMENTATION)"
#debug
echo "Debug: codeblock ..."
echo ""


# prohibited files: (file search pattern: harrisburg-linux and wannacrypt)
echo "Prohibited media files:"

# search for full list (wannacrypt)
find / -iname "*.doc" -o -iname "*.docx" -o -iname "*.xls"\
-o -iname "*.xlsx" -o -iname "*.ppt" -o -iname "*.pptx"\
-o -iname "*.pst" -o -iname "*.ost" -o -iname "*.msg"\
-o -iname "*.eml" -o -iname "*.vsd" -o -iname "*.vsdx"\
-o -iname "*.txt" -o -iname "*.csv" -o -iname "*.rtf"\
-o -iname "*.123" -o -iname "*.wks" -o -iname "*.wk1"\
-o -iname "*.pdf" -o -iname "*.dwg" -o -iname "*.onetoc2"\
-o -iname "*.snt" -o -iname "*.jpeg" -o -iname "*.jpg"\
-o -iname "*.docb" -o -iname "*.docm" -o -iname "*.dot"\
-o -iname "*.dotm" -o -iname "*.dotx" -o -iname "*.xlsm"\
-o -iname "*.xlsb" -o -iname "*.xlw" -o -iname "*.xlt"\
-o -iname "*.xlm" -o -iname "*.xlc" -o -iname "*.xltx"\
-o -iname "*.xltm" -o -iname "*.pptm" -o -iname "*.pot"\
-o -iname "*.pps" -o -iname "*.ppsm" -o -iname "*.ppsx"\
-o -iname "*.ppam" -o -iname "*.potx" -o -iname "*.potm"\
-o -iname "*.edb" -o -iname "*.hwp" -o -iname "*.602"\
-o -iname "*.sxi" -o -iname "*.sti" -o -iname "*.sldx"\
-o -iname "*.sldm" -o -iname "*.sldm" -o -iname "*.vdi"\
-o -iname "*.vmdk" -o -iname "*.vmx" -o -iname "*.gpg"\
-o -iname "*.aes" -o -iname "*.ARC" -o -iname "*.PAQ"\
-o -iname "*.bz2" -o -iname "*.tbk" -o -iname "*.bak"\
-o -iname "*.tar" -o -iname "*.tgz" -o -iname "*.gz"\
-o -iname "*.7z" -o -iname "*.rar" -o -iname "*.zip"\
-o -iname "*.backup" -o -iname "*.iso" -o -iname "*.vcd"\
-o -iname "*.bmp" -o -iname "*.png" -o -iname "*.gif"\
-o -iname "*.raw" -o -iname "*.cgm" -o -iname "*.tif"\
-o -iname "*.tiff" -o -iname "*.nef" -o -iname "*.psd"\
-o -iname "*.ai" -o -iname "*.svg" -o -iname "*.djvu"\
-o -iname "*.m4u" -o -iname "*.m3u" -o -iname "*.mid"\
-o -iname "*.wma" -o -iname "*.flv" -o -iname "*.3g2" \
-o -iname "*.mkv" -o -iname "*.3gp" -o -iname "*.mp4"\
-o -iname "*.mov" -o -iname "*.avi" -o -iname "*.asf"\
-o -iname "*.mpeg" -o -iname "*.vob" -o -iname "*.mpg"\
-o -iname "*.wmv" -o -iname "*.fla" -o -iname "*.swf"\
-o -iname "*.wav" -o -iname "*.mp3" -o -iname "*.sh"\
-o -iname "*.class" -o -iname "*.jar" -o -iname "*.java"\
-o -iname "*.rb" -o -iname "*.asp" -o -iname "*.php"\
-o -iname "*.jsp" -o -iname "*.brd" -o -iname "*.sch"\
-o -iname "*.dch" -o -iname "*.dip" -o -iname "*.pl"\
-o -iname "*.vb" -o -iname "*.vbs" -o -iname "*.ps1"\
-o -iname "*.bat" -o -iname "*.cmd" -o -iname "*.js"\
-o -iname "*.asm" -o -iname "*.h" -o -iname "*.pas"\
-o -iname "*.cpp" -o -iname "*.c" -o -iname "*.cs"\
-o -iname "*.suo" -o -iname "*.sln" -o -iname "*.ldf"\
-o -iname "*.mdf" -o -iname "*.ibd" -o -iname "*.myi"\
-o -iname "*.myd" -o -iname "*.frm" -o -iname "*.odb"\
-o -iname "*.dbf" -o -iname "*.db" -o -iname "*.mdb"\
-o -iname "*.accdb" -o -iname "*.sql" -o -iname "*.sqlitedb"\
-o -iname "*.sqlite3" -o -iname "*.asc" -o -iname "*.lay6"\
-o -iname "*.lay" -o -iname "*.mml" -o -iname "*.sxm"\
-o -iname "*.otg" -o -iname "*.odg" -o -iname "*.uop"\
-o -iname "*.std" -o -iname "*.sxd" -o -iname "*.otp"\
-o -iname "*.odp" -o -iname "*.wb2" -o -iname "*.slk"\
-o -iname "*.dif" -o -iname "*.stc" -o -iname "*.sxc"\
-o -iname "*.ots" -o -iname "*.ods" -o -iname "*.3dm"\
-o -iname "*.max" -o -iname "*.3ds" -o -iname "*.uot"\
-o -iname "*.stw" -o -iname "*.sxw" -o -iname "*.ott"\
-o -iname "*.odt" -o -iname "*.pem" -o -iname "*.p12"\
-o -iname "*.csr" -o -iname "*.crt" -o -iname "*.key"\
-o -iname "*.pfx" -o -iname "*.der" 2>/dev/null |sort >prohibited-file-list.log 2>/dev/null
chmod 555 ./prohibited-file-list.log

# debug
# search within /home directory (media files only)
#find /home -iname "*.mp3" -o -iname "*.mov" -o -iname "*.mp4" \
#o -iname "*.avi" -o -iname "*.mpg" -o -iname "*.mpeg" \
#-o -iname "*.flac" -o -iname "*.m4a" -o -iname "*.flv" \
#-o -iname "*.ogg" -o -iname "*.gif" -o -iname "*.png" \
#-o -iname "*.jpg" -o -iname "*.jpeg"

echo ""
echo "Prohibited file search done. For full list see prohibited-file-list.log"


echo ""
echo "------ End Section ------"
printf "\n\n\n"



#------ Advanced hardening ------
echo "------ Advanced hardening ------"
echo ""


# SSH hardening
echo "Hardening SSH configuration: (pubkey authentication not configured)"

# deduplication canary
if [ -e /etc/ssh/sshd_config -a "$(tail -n1 /etc/ssh/sshd_config)" != "# autodit-ssh was here..." ]; then
	cp -na /etc/ssh/sshd_config /etc/ssh/sshd_config_bak

	echo "" >> /etc/ssh/sshd_config
	echo "" >> /etc/ssh/sshd_config
	echo "# autodit custom config" >> /etc/ssh/sshd_config

	# comment out relevant settings
	sed 's/^\(AllowTcpForwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(ClientAliveCountMax\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(ClientAliveInterval\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Compression\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(GatewayPorts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(IgnoreRhosts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(LoginGraceTime\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(LogLevel\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MaxAuthTries\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MaxSessions\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitRootLogin\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitUserEnvironment\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitTunnel\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PrintLastLog\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(StrictModes\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(TCPKeepAlive\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(UseDNS\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(X11Forwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(AllowAgentForwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitEmptyPasswords\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PubkeyAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PasswordAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Protocol\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(HashKnownHosts\)/\#\1/gI' -i /etc/ssh/sshd_config
	
	# change settings through append
	echo "AllowTcpForwarding no" >> /etc/ssh/sshd_config
	echo "ClientAliveCountMax 2" >> /etc/ssh/sshd_config
	echo "ClientAliveInterval 300" >> /etc/ssh/sshd_config
	echo "Compression no" >> /etc/ssh/sshd_config
	echo "GatewayPorts no" >> /etc/ssh/sshd_config
	echo "IgnoreRhosts yes" >> /etc/ssh/sshd_config
	echo "LoginGraceTime 1m" >> /etc/ssh/sshd_config
	echo "LogLevel VERBOSE" >> /etc/ssh/sshd_config
	echo "MaxAuthTries 2" >> /etc/ssh/sshd_config
	echo "MaxSessions 2" >> /etc/ssh/sshd_config
	echo "PermitRootLogin no" >> /etc/ssh/sshd_config
	echo "PermitUserEnvironment no" >> /etc/ssh/sshd_config
	echo "PermitTunnel no" >> /etc/ssh/sshd_config
	echo "PrintLastLog yes" >> /etc/ssh/sshd_config
	echo "StrictModes yes" >> /etc/ssh/sshd_config
	echo "TCPKeepAlive no" >> /etc/ssh/sshd_config
	echo "UseDNS no" >> /etc/ssh/sshd_config
	echo "X11Forwarding no" >> /etc/ssh/sshd_config
	echo "AllowAgentForwarding no" >> /etc/ssh/sshd_config
	echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config
#	echo "PubkeyAuthentication\ yes" >> /etc/ssh/sshd_config		# uncomment if use pubkey
#	echo "PasswordAuthentication\ no" >> /etc/ssh/sshd_config		# uncomment if use pubkey
	echo "Protocol 2" >> /etc/ssh/sshd_config
#	echo "HashKnownHosts yes" >> /etc/ssh/sshd_config

	# canary bit
	echo "# autodit-ssh was here..." >> /etc/ssh/sshd_config # && echo "debug autodit-ssh" # debug

	systemctl restart sshd.service || echo "Something went wrong with SSH. Check /etc/ssh/sshd_config!"

fi
echo ""



# Kernel hardening
echo "Hardening kernel: "

cp -na /etc/sysctl.conf /etc/sysctl.conf_bak

# deduplication canary
if [ -e /etc/sysctl.conf -a "$(head -n1 /etc/sysctl.conf)" != "# autodit was here..." ]; then

# comment out relevant settings
	sed -e 's/^\(fs\.protected_hardlinks\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(fs\.protected_symlinks\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(fs\.suid_dumpable\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.core_uses_pid\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.ctrl-alt-del\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.dmesg_restrict\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(kernel\.kptr_restrict\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.randomize_va_space\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.sysrq\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.yama.ptrace_scope\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4.conf\.all\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.conf\.all\.bootp_relay\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.log_martians\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.mc_forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.proxy_arp\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.rp_filter\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.conf\.all\.send_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.log_martians\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.icmp_echo_ignore_broadcasts\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.icmp_ignore_bogus_error_responses\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.tcp_syncookies\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.tcp_timestamps\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf

	# change settings through append
	echo "" >> /etc/sysctl.conf
	echo "" >> /etc/sysctl.conf
	echo "# autodit kernel hardening configurations:" >> /etc/sysctl.conf
	
	echo "fs.protected_hardlinks = 1" >> /etc/sysctl.conf
	echo "fs.protected_symlinks = 1" >> /etc/sysctl.conf
	echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf
	echo "kernel.core_uses_pid = 1" >> /etc/sysctl.conf
	echo "kernel.ctrl-alt-del = 0" >> /etc/sysctl.conf
	echo "kernel.dmesg_restrict = 1" >> /etc/sysctl.conf

	echo "kernel.kptr_restrict = 2" >> /etc/sysctl.conf
	echo "kernel.randomize_va_space = 2" >> /etc/sysctl.conf
	echo "kernel.sysrq = 0" >> /etc/sysctl.conf
	echo "kernel.yama.ptrace_scope = 2" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.accept_source_route = 0" >> /etc/sysctl.conf

	echo "net.ipv4.conf.all.bootp_relay = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.log_martians = 1" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.mc_forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.proxy_arp = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.rp_filter = 1" >> /etc/sysctl.conf

	echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.log_martians = 1" >> /etc/sysctl.conf
	echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" >> /etc/sysctl.conf
	echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" >> /etc/sysctl.conf

	echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_timestamps = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.accept_source_route = 0" >> /etc/sysctl.conf

	# deduplication canary
	sed -i /etc/sysctl.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' && echo "DONE" # && echo "debug kernel-harden" #debug

else
	echo "Kernel was hardened by autodit. Append check additional configurations manually!"

fi

echo ""


# Network hardening
echo "Hardneing network settings: "

cp -na /etc/host.conf /etc/host.conf_bak

# deduplication canary
if [ -e /etc/host.conf -a "$(head -n1 /etc/host.conf)" != "# autodit was here..." ]; then

	echo "nospoof on" >> /etc/host.conf
	# deduplication canary
	sed -i /etc/host.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' && echo "DONE" # && echo "debug host.conf" #debug

else
	echo "Network settings were hardened by autodit. Append check additional configurations manually!"

fi


echo ""
echo "------ End Section ------"
printf "\n\n\n"
