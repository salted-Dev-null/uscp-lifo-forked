#!/bin/bash

# NOTE TO DEVS:
# FIX PASSWORD PROTECT TO ONLY EDITING, CMD AND RECOVERY!

# Credits:

# Table of contents: (search to jump!)					(CORRECT FORMAT PENDING! END;HEAD;SEC)
#	generate password
#	append custom grub.d configurations
#		add superuser and password
#		protect linux recovery modes	(DEBUG PENDING!)
#		update grub configuration



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



#------ generate password ------
echo "------ Generate grub2 password ------"
grub-mkpasswd-pbkdf2|tee -a ./localpw
pwHash="$(sudo tail -n+3 localpw |sed -e 's/.*grub/grub/')"
rm ./localpw
echo ""
echo "------ Generate grub2 password end------"
printf "\n\n\n"

# debug
#echo ""
#echo ""
#echo $pwHash

#------ generate password end ------



#------ append custom grub.d configurations ------
echo "------ append custom grub.d configurations ------"
echo ""


# deduplication canary
if [ "$(head -n2 /etc/grub.d/40_custom)" != "# autodit was here..." ]; then

	# add superuser and password
	echo "Adding grub2 superuser and password:"
	echo "set superusers=\"root\"" >> /etc/grub.d/40_custom
	echo "password_pbkdf2 root $pwHash" >> /etc/grub.d/40_custom
	echo "DONE"
	echo ""
	
	# protect linux recovery modes
	echo "Password protecting linux-recovery menuentries:"
	# Debug comment
	#echo "sed -i /etc/grub.d/40_custom -E -e 's/(\t)(menuentry)(.*)(\{.*)/\1\2\3--users \"\" \4/g'" >> /etc/grub.d/40_custom
	# BROKEN! Requires FIX!
	echo DONE
	echo ""

	# update grub configuration
	echo "Updating grub2 configuration file:"
	update-grub2
	echo DONE
	echo ""

	# canary bit
	sed -i /etc/grub.d/40_custom -e '2i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug grub" #debug

else

	echo "grub2 password was added by autodit..."

fi

echo "------ append custom grub.d configurations end ------"
printf "\n\n\n"
#------ append custom grub.d configurations end ------
