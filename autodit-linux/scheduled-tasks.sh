#!/bin/bash

# NOTE TO DEVS:
# LS ANA/CRONTAB SCRIPTS WHEN CHECKING /etc/cron* AND PROMT (DONE)

# Credits:

# Table of contents: (search to jump!)
#	Scheduled tasks
#		ana/cron check
#			user specific crontabs
#			system-wide crontab
#			anacron check
#			cron.d and cron.<period> tasks
#		systemd timer check

# (c) 2019-2020, salted-Dev-null, All Rights Reserved



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



# install prerequisite
apt-get install systemd --reinstall -y



#------ Scheduled tasks ------
echo "------ Scheduled tasks ------"
echo ""
echo ""


# cron check
# user specific crontabs
echo "crontab for all users:"
for user in $(cut -f1 -d: /etc/passwd); do 
	echo "crontab for $user: "
	crontab -u $user -l; 
	echo ""
done
echo ""

# system-wide crontab
echo "Manually check system-wide crontab"
echo "Press enter to continue:"
read
less /etc/crontab
echo ""

# anacrontab check
echo "Checking anacrontab: "
echo "Press enter to continue:"
read
less /etc/anacrontab
echo ""

# cron.d and cron.<period> tasks
echo "ana/cron tasks:"
echo "Check suspicious tasks with {less /etc/cron.d/suspicious-task}"
echo "-- and --"
echo "{less /etc/cron.<period>/suspicious-task}"
echo "Tasks:"
ls --color=auto -lA /etc/cron.*/*
echo "Rerun {less /etc/cron.*/*} to manually inspect cron.* again."
echo ""

# systemd timer check
echo "systemd timers: "
echo "See systemd.unit(5) for locations: "
systemctl list-timers --all --no-pager


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Scheduled tasks End ------
