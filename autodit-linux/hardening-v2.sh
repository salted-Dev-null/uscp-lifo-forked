#!/bin/bash

#NOTES:
# REMOVE LOGGING STDIO REDIRECTS!
# REMOVE SECTIONING!


# TEMPLATES:
# apt-get remove:
#	apt-get -q -y remove WTF && echo "WTF removed." || echo "Something went wrong removing WTF..."
#	apt-get -q -y remove WTF && echo "DONE" || echo "Something went wrong..."
# apt-get install
#	apt-get install -q -y WTF &>/dev/null && echo "DONE" || echo "Something went wrong..."

# (c) 2019-2020, salted-Dev-null, All Rights Reserved

driver(){
# main function
# comment out modules to customize functionality
checkroot	# do not comment out!
tips
FQ_prompt
dep_hell
prereq
firewall
av
maldet
motd
cups_conf
lock_pwless
lock_root
disable_guest
pw_age_and_umask
pw_history_complexity
pw_lockout
ssh_harden		# optional toggles
kernel_harden		# optional toggles
#network_harden		# disabled bc deprecated
#grub_harden		# disabled bc remote access...
fs_harden
coredump_harden
#mounts_harden		# can **** up system beyond remote access...
autofs_harden
sticky_harden
prelink_harden
inetd_harden
ntp_harden
service_harden
service_client_harden
tcpwrapper_harden
netproto_harden
cron_harden
anacron_harden
#systemd_harden		# incomplete, might break ****
user_harden
bash_harden
su_harden
perms_harden
#auditd_harden		# ****ING BROKEN... # LEAVE THIS LAST as script hardening WILL trigger auditd. will generate script completion timestamp for future reference
#final_tips

}



checkroot(){
# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi
}



cont(){
echo "Press any key to coninue or Ctrl+C to quit:"
read
}



c_dir(){
# get current directory
BASEDIR=$(dirname "$0")
LOGDIR="$BASEDIR/hardening-logs"
mkdir "$LOGDIR"
}


tips(){
# general tips
echo "------ General Auditing Tips ------"
echo ""
echo "READ company policies and forensics questions first!!!"
echo "General streamlined procedure:"
echo "Firewalling --> Install av and ids --> User auditing --> "
echo "Configure auto updates --> Install Security Updates --> "
echo ""
echo "------ End Section ------"
echo ""
#cont
}



final_tips(){
# final tips
echo "------ Final Notes ------"
echo ""
echo "Don't forget to:"
echo "1: Turn on auto updates in GUI"
echo "2: Turn on auto security updates in GUI"
echo "3: Get latest version of lynis, chkrootkit and rkhunter then SCAN (including clamav/tk)" 
echo "4: Update and upgrade system! (Takes a buttload of time.)"
echo "5: GLHF! SKIP IF PAST 20MIN TIME!"
echo ""
echo "------ Final Notes End ------"
echo ""
#cont
}



FQ_prompt(){
# promt for forensics questions
echo "Did you FINISH ANSWERING forensics questions?!"
echo "Type UPPERCASE yes to proceed: "
read proceed

if [ "$proceed" != "YES" ]; then
	echo ""
	echo "------ Warning ------"
	echo "PLEASE COMPLETE FORENSICS QUESTIONS FIRST THEN READ COMPANY POLICIES!"
	echo "------ Warning ------"
	echo ""
	exit 1
fi
echo ""
}



dep_hell(){
# update software sources
echo "Updating software sources" 
apt-get update -q -y && echo "DONE" || { echo "Something went wrong! We're in terrible terrible denpendency hell!"; exit 1; }
echo "Installing unattended-upgrades and apt-listchanges"
apt-get install -q -y apt-listchanges unattended-upgrades &>/dev/null && echo "DONE" || echo "Something went wrong..."
echo ""
#cont
}



prereq(){
echo "Installing prerequisite software:" 
apt-get install -q -y 'needrestart' &>/dev/null && echo DONE || echo "Prerequisite installation not successful! Please check!"
echo ""
#cont
}



firewall(){
# setup firewall

# install iptables
#echo "Installing iptables:"
#apt-get install -q -y iptables iptables-persistent && echo DONE || echo "iptables installation not successful! Please check!"

# check if ufw installed
if [ ! -x /usr/sbin/ufw ]; then
	
	echo "Installing g/ufw:" 
	
	# gui prompt
	firewall_gui_bit="false"
	echo "Install gufw? (Yes/no):"
	read firewall_gui_bit;
	firewall_gui_bit_result=$(echo "$firewall_gui_bit"|egrep -Ei '^y')

	if [ -n "$firewall_gui_bit_result" ]; then
		apt-get install -q -y ufw gufw &>/dev/null && echo DONE || echo "G/UFW installation not successful! Please check!" 
	else
		apt-get install -q -y ufw &>/dev/null && echo DONE || echo "UFW installation not successful! Please check!" 
	fi

fi

echo "Setting up ufw:"
echo "Allowing ssh acccess..."
ufw allow ssh || echo "Something went wrong..."
echo "Reject all incoming..."
ufw default reject incoming || echo "Something went wrong..."
echo "Setup logging (MEDIUM)..."
ufw logging on || echo "Something went wrong..."
ufw logging medium || echo "Something went wrong..."
# backup /etc/default/ufw
cp -na /etc/default/ufw /etc/default/ufw_bak
echo "Setting up IPv6 support:"
sed -E -e 's/^(IPV6=)(.*)/\1yes/' -i /etc/default/ufw &>/dev/null && echo "DONE" || echo "Something went wrong..."
echo "Enabling firewall..."
ufw enable || echo "Something went wrong. Please check firewall configuration!"
echo ""
#cont
}



av(){
# install clamav
echo "Installing clamav: (don't forget to configure auto scan in clamav!)"

# check if installed
if [ -x /usr/bin/clamtk -a -x /usr/bin/clamscan ]; then
	echo "clamav and clamtk is already installed."
elif [ -x /usr/bin/clamscan ]; then
	echo "clamav is already installed."
	echo "install clamav gui manually."
else
	# prompt for gui install or not
	av_gui_bit="null"
	echo "Install gui clamav? (Yes/no):"
	read av_gui_bit;
	av_gui_bit_result=$(echo "$av_gui_bit"|egrep -Ei '^y')

	# debug comment
	#echo $av_gui_bit_result
	
	
	if [ -n "$av_gui_bit_result" ]; then
		apt-get install -q -y clamav clamtk &>/dev/null && echo DONE || echo "clamav/tk installation not successful! Please check!"
	else
		apt-get install -q -y clamav &>/dev/null && echo DONE || echo "clamav installation not successful! Please check!"
	fi
fi
echo ""
#cont
}



maldet(){
# install LMD

# make maldet install dir and enter
mkdir maldet-dir
cd maldet-dir

echo "Installing LMD:"

# download and integrity check
wget http://www.rfxn.com/downloads/maldetect-current.tar.gz
# integrity check
correctSum="6a1226a1892e557f822975060ecfead795476183e31c650908f52c7b87e0fa84  maldetect-current.tar.gz" #sha256 digest, last updated Sun May  5 23:38:47 PDT 2019
sha256sum maldetect-current.tar.gz>dlSum
echo "$correctSum" > ./corSum
result=$(diff corSum dlSum)

#debug
#echo "$result"

if [ -n "$result" ]; then
	echo "Download sha256 sum is incorrect! exiting LMD installation"
elif [ -z "$result" ]; then
	echo "Download sha256 sum is correct! proceeding with LMD installation"

	tar -xzf maldetect-current.tar.gz
	cd maldetect-*/
	bash ./install.sh && echo "LMD is installed." || echo "Something went wrong installing LMD! Please check!"
	cd ../

fi

# exit maldet install dir and remove
cd ..
rm -rf maldet-dir

echo ""
#cont
}



motd(){
# Legal Banner
echo "Editing legal banner!"
cp -na /etc/issue /etc/issue_bak
cp -na /etc/issue.net /etc/issue.net_bak
cp -na /etc/motd /etc/motd_bak

# banner variable
bannerText="\n\
|-----------------------------------------------|\n\
|   WARNING: THIS COMPUTER IS PROPERTY OF AFA   |\n\
|   THIS COMPUTER IS FOR AUTHORIZED USE ONLY!   |\n\
|          YOUR ACTIONS ARE MONITORED!          |\n\
|-----------------------------------------------|\n\
"

# motd banner
motdText="This is a US Air Force Association computer system for authorized use only. AFA computer \
systems may be monitored for all lawful purposes, including to ensure that their use is authorized, \
for management of the system, to facilitate against unauthorized access, and to verify security \
procedures, survivability, and operational security. Using this system constitutes consent to \
monitoring. All information, including personal information, placed on or sent over this system may \
be obtained during monitoring. Unauthorized use could result in criminal prosecution."

# deduplication canary for /etc/issue
if [ -z "$(egrep -E '^[|][-]*[|]' /etc/issue)" ]; then
	echo -e "$bannerText" > /etc/issue
	echo "DONE /etc/issue"
else
	echo "/etc/issue is already configured"
fi
# deduplication canary for /etc/issue.net
if [ -z "$(egrep -E '^[|][-]*[|]' /etc/issue.net)" ]; then
	echo -e "$bannerText" > /etc/issue.net
	echo "DONE /etc/issue.net"
else
	echo "/etc/issue.net is already configured"
fi
# deduplication canary for /etc/motd
if [ -z "$(egrep -E 'criminal prosecution' /etc/motd)" ]; then
	echo -e "$motdText" > /etc/motd
	echo "DONE /etc/motd"
else
	echo "/etc/motd is already configured"
fi
# change banner permissions
chown root:root /etc/issue.net /etc/issue.net_bak /etc/issue /etc/issue_bak /etc/motd /etc/motd_bak
chmod 644 /etc/issue.net /etc/issue.net_bak /etc/issue /etc/issue_bak /etc/motd /etc/motd_bak

echo ""
#cont
}



cups_conf(){
# cupsd.conf permissions

# check if installed
if [ -e /etc/cups/cupsd.conf ]; then
	echo "Changing /etc/cups permissions:"
	chown root:root /etc/cups/ && chmod og-rwx /etc/cups/ && echo "DONE" || "something went wrong..."
else
	echo "cups isn't installed"
fi
echo ""
#cont
}



lock_pwless(){
# declare variables here!!!
listFields=0
currentUser=null

# get passwordless users
npUsers=$(passwd -Sa| grep " NP " |cut -f1 -d" "|tr "\n" ",") # redhat breaks
listFields=$(echo $npUsers |grep -o ","|wc -l)
currentUser=null

# lock passwordless users
echo "Locking passwordless users: "

for i in `seq 1 $listFields`
do
	currentUser=$(echo "$npUsers"| cut -f$i -d",")
	passwd -l "$currentUser" &>/dev/null || echo "Something went wrong locking $currentUser!"
done
echo "DONE"
echo ""
#cont
}



lock_root(){
# lock root user
echo "Locking down root:"
passwd -l root &>/dev/null && echo "DONE" || echo "problem locking local root..."

echo ""
#cont
}



disable_guest(){
# disable guest users
echo "Disabling guest account:"
disableGuest=0

if [ -e /etc/lightdm/lightdm.conf ]; then
	echo "Detected lightdm..."
	
	#backup lightdm.conf
	cp -na /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf_bak
	
	# lightdm.conf perms
	chown root:root /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf_bak
	chmod og-wx /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf_bak

	# deduplication canary
	if [ "$(head -n1 /etc/lightdm/lightdm.conf)" != "# autodit was here..." ]; then
		echo "allow-guest=false">>/etc/lightdm/lightdm.conf
		# canary bit
		sed -i /etc/lightdm/lightdm.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug lightdm" #debug
	fi

	disableGuest=1
	echo "Manually restart lightdm later! (sudo systemctl restart lightdm.service)"
fi

# GDM INVESTIGATION PENDING!

if [ "$disableGuest" == 1 ]; then
	echo "Guest account disabled."
else
	echo "Lightdm not detected, please manually disable guest if applicable!"
fi
echo ""
#cont
}



pw_age_and_umask(){
# password age policy
echo "Changing password aging and default umask in login.defs:"

# backup login.defs
cp -na /etc/login.defs /etc/login.defs_bak

# restrict perms
chown root:root /etc/login.defs /etc/login.defs_bak
chmod og-wx /etc/login.defs /etc/login.defs_bak

# deduplication canary
if [ "$(head -n1 /etc/login.defs)" != "# autodit was here..." ]; then		# redhat breaks
	sed -e "s/PASS_MIN_DAYS.*/PASS_MIN_DAYS\ 0/g" -i /etc/login.defs
	sed -e "s/PASS_MAX_DAYS.*/PASS_MAX_DAYS\ 90/g" -i /etc/login.defs
	sed -e "s/PASS_WARN_AGE.*/PASS_WARN_AGE\ 14/g" -i /etc/login.defs

	# harden access controls
	sed -i /etc/login.defs -r -e 's/^(UMASK)([^0-9]*)([0-9]*)/\1\2027/gi'

	# canary bit
	sed -i /etc/login.defs -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug login.defs" #debug
	
	echo "DONE"
else
	echo "password aging and default umask already configured..."
fi

# notes
echo "Notes: use chpasswd to batch change passwords"

echo ""
#cont
}



# pw_history_complexity(){			#OLD CODEBLOCK!!! DO NOT USE!!! ****S UP LOGIN!!!
# # pam password complexity and history policy
# echo "Configuring PAM.d password and auth policies:"
# apt-get install -q -y libpam-cracklib &>/dev/null && echo "libpam installed." || echo "Something went wrong installing libpam..."
# cp -na /etc/pam.d/common-password /etc/pam.d/common-password_bak
# 
# # deduplication canary
# if [ "$(head -n1 /etc/pam.d/common-password)" != "# autodit was here..." ]; then
# 	sed -e 's/\(pam_unix\.so.*\)/\1 remember=5/g' -i /etc/pam.d/common-password	# redhat breaks
# 	sed -e "s/\(pam_cracklib\.so.*\)/\1 ucredit=-1\ lcredit=-1\ dcredit=-1\ ocredit=-1\ enforce_for_root/g" -i /etc/pam.d/common-password
# 	
# 	# check if minlen exists and if its value is above 8
# 	checkString="$(cat /etc/pam.d/common-password|grep -i pam_cracklib|grep -i minlen)"
# 	checkLength=$(echo "$checkString"|sed -r -e 's/.*minlen=([0-9]*).*/\1/')
# 	if [ -z "$checkString" ]; then
# 		sed -r -e "s/(.*pam_cracklib\.so.*)/\1 minlen=8" -i /etc/pam.d/common-password
# 	elif [ "$checkLength" -lt "8" ]; then
# 		sed -r -e 's/(.*)(minlen=[0-9]*)(.*)/\1minlen=8\3/' -i /etc/pam.d/common-password
# 	fi
# 	
# 	# canary bit
# 	sed -i /etc/pam.d/common-password -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug common-password" #debug
# 	
# 	echo "DONE"
# else
# 	echo "libpam is already configured..."
# fi
# 
# 
# echo ""
# #cont
# }



pw_history_complexity(){
# pam password complexity and history policy
echo "Configuring PAM.d password and auth policies:"
# apt-get install -q -y libpam-pwquality &>/dev/null && echo "libpam-pwquality installed." || echo "Something went wrong installing libpam-pwquality..."	# comment out bc interactive dialogue stuck bug
apt-get install -q -y libpam-pwquality &>/dev/null && echo "libpam-pwquality installed." || echo "Something went wrong installing libpam-pwquality..."

# backup configuration files
cp -na /etc/pam.d/common-password /etc/pam.d/common-password_bak
cp -na /etc/security/pwquality.conf /etc/security/pwquality.conf_bak

# restrict perms
chown root:root /etc/pam.d/common-password /etc/pam.d/common-password_bak /etc/security/pwquality.conf /etc/security/pwquality.conf_bak
chmod og-wx /etc/security/pwquality.conf /etc/security/pwquality.conf_bak

# deduplication canary
if [ -z "$(cat /etc/security/pwquality.conf|grep -i pw_history_complexity)" ]; then
	
	# canary
	echo "" >> /etc/security/pwquality.conf  || pwcomplex_bit="false"
	echo "" >> /etc/security/pwquality.conf || pwcomplex_bit="false"
	echo "# Autodit pw_history_complexity was here..." >> /etc/security/pwquality.conf || pwcomplex_bit="false"
	
	# comment out existing 
	sed -E -e 's/^(minlen)/#\1/g' -i /etc/security/pwquality.conf || pwcomplex_bit="false"
	sed -E -e 's/^(dcredit)/#\1/g' -i /etc/security/pwquality.conf || pwcomplex_bit="false"
	sed -E -e 's/^(ucredit)/#\1/g' -i /etc/security/pwquality.conf || pwcomplex_bit="false"
	sed -E -e 's/^(ocredit)/#\1/g' -i /etc/security/pwquality.conf || pwcomplex_bit="false"
	sed -E -e 's/^(lcredit)/#\1/g' -i /etc/security/pwquality.conf || pwcomplex_bit="false"
	
	# change complexity requriements
	echo "Hardening pwquality.conf: "
	pwcomplex_bit="true"
	# change complexity in /etc/security/pwquality.conf
	echo "minlen = 14" >> /etc/security/pwquality.conf || pwcomplex_bit="false"
	echo "dcredit = -1" >> /etc/security/pwquality.conf || pwcomplex_bit="false"
	echo "ucredit = -1" >> /etc/security/pwquality.conf || pwcomplex_bit="false"
	echo "ocredit = -1" >> /etc/security/pwquality.conf || pwcomplex_bit="false"
	echo "lcredit = -1" >> /etc/security/pwquality.conf || pwcomplex_bit="false"
		
	if [ "$pwcomplex_bit" == "false" ]; then
		echo "pwquality.conf went wrong..."
	else
		echo "pwquality.conf DONE"
	fi
	
else
	echo "pwquality.conf is already configured..."
fi

# deduplication canary
if [ -z "$(cat /etc/pam.d/common-password|grep -i pw_history_complexity)" ]; then
	
	# canary
	echo "" >> /etc/pam.d/common-password
	echo "" >> /etc/pam.d/common-password
	echo "# Autodit pw_history_complexity was here..." >> /etc/pam.d/common-password
	
	# change common-password rules
	echo "Hardening common-password:"
	# insert pam_pwhistory rule before pam_unix checker
	pwhistory_bit="true"
	# pam_pwhistory
	sed -E -e 's/^(password.*pam_unix\.so.*)/password\trequired\tpam_pwhistory.so remember=5\n\1/g' -i /etc/pam.d/common-password || pwhistory_bit="false"
	# pam_pwquality
	sed -E -e 's/^(password)(.*pam_pwquality.so.*)/\1\2 enforce_for_root/g' -i /etc/pam.d/common-password || pwhistory_bit="false"

	if [ "$pwhistory_bit" == "false" ]; then
		echo "common-password went wrong..."
	else
		echo "common-password DONE"
	fi
else
	echo "common-password is already configured..."
fi


echo ""
#cont
}



pw_lockout(){
# account lockout policy
echo "Configuring account lockout policies:"

# backup configurations
cp -na /etc/pam.d/common-auth /etc/pam.d/common-auth_bak

# restrict perms
chown root:root /etc/pam.d/common-auth /etc/pam.d/common-auth_bak
chmod og-wx /etc/pam.d/common-auth /etc/pam.d/common-auth_bak

# deduplication canary
if [ "$(head -n1 /etc/pam.d/common-auth)" != "# autodit was here..." ]; then	# redhat breaks

	# insert tally2 rule before deny
	sed -i /etc/pam.d/common-auth -e 's/\(.*pam_deny.so.*\)/auth\trequired\tpam_tally2.so audit silent deny=5 onerr=fail unlock_time=1800\n\1/g'
	
	# increase line skip on success to accommodate above rule
	successSkip=$(grep -i "success" /etc/pam.d/common-auth|sed -r -e "/#/d" -e "s/(.*success=)([0-9]*)(.*)/\2/")
	successSkip=$((successSkip+1))
	sed -r -i /etc/pam.d/common-auth -e "s/success=[0-9]*/success=$successSkip/"
	
	# canary bit
	sed -i /etc/pam.d/common-auth -e '1i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug common-auth" #debug
	
	echo "DONE"
else
	echo "Account lockout policies already configured..."
fi


echo ""
#cont
}



ssh_harden(){
# SSH hardening
echo "Hardening SSH configuration: (pubkey authentication not configured)"

# backup sshd_config
cp -na /etc/ssh/sshd_config /etc/ssh/sshd_config_bak

# change perms on /etc/ssh/ and sshd_config
chown root:root /etc/ssh/ /etc/ssh/sshd_config /etc/ssh/sshd_config_bak
chmod og-w /etc/ssh/ 
chmod og-rwx /etc/ssh/sshd_config /etc/ssh/sshd_config_bak
# chagne perms on host private keys
find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec chown root:root {} \;
find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec chmod 0600 {} \;
# chagne perms on host public keys
find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chmod 0644 {} \;
find /etc/ssh -xdev -type f -name 'ssh_host_*_key.pub' -exec chown root:root {} \;



# deduplication canary
if [ -e /etc/ssh/sshd_config -a "$(tail -n1 /etc/ssh/sshd_config)" != "# autodit-ssh was here..." ]; then
	
	# canary
	echo "" >> /etc/ssh/sshd_config
	echo "" >> /etc/ssh/sshd_config
	echo "# autodit custom config" >> /etc/ssh/sshd_config

	# comment out relevant settings
	sed 's/^\(AllowTcpForwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(ClientAliveCountMax\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(ClientAliveInterval\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Compression\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(GatewayPorts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(IgnoreRhosts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(LoginGraceTime\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(LogLevel\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MaxAuthTries\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MaxSessions\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitRootLogin\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitUserEnvironment\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitTunnel\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PrintLastLog\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(StrictModes\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(TCPKeepAlive\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(UseDNS\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(X11Forwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(AllowAgentForwarding\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitEmptyPasswords\)/\#\1/gI' -i /etc/ssh/sshd_config
#	sed 's/^\(PubkeyAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config	# uncomment if use pubkey
#	sed 's/^\(PasswordAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config	# uncomment if use pubkey
	sed 's/^\(Protocol\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(HashKnownHosts\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Banner\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(DebianBanner\)/\#\1/gI' -i /etc/ssh/sshd_config		# redhat breaks, comment if rh distros
#	sed 's/^\(VersionAddendum\)/\#\1/gI' -i /etc/ssh/sshd_config		# redhat fix, uncomment if rh distros
	# CIS benchmarks start
	sed 's/^\(Protocol\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(HostbasedAuthentication\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(PermitUserEnvironment\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(Ciphers\)/\#\1/gI' -i /etc/ssh/sshd_config
	sed 's/^\(MACs\)/\#\1/gI' -i /etc/ssh/sshd_config
#	sed 's/^\(KexAlgorithms\)/\#\1/gI' -i /etc/ssh/sshd_config		# invalid kex algos included, pending fix!

	# change settings through append
	echo "AllowTcpForwarding no" >> /etc/ssh/sshd_config
	echo "ClientAliveCountMax 2" >> /etc/ssh/sshd_config
	echo "ClientAliveInterval 300" >> /etc/ssh/sshd_config
	echo "Compression no" >> /etc/ssh/sshd_config
	echo "GatewayPorts no" >> /etc/ssh/sshd_config
	echo "IgnoreRhosts yes" >> /etc/ssh/sshd_config
	echo "LoginGraceTime 1m" >> /etc/ssh/sshd_config
	echo "LogLevel INFO" >> /etc/ssh/sshd_config
	echo "MaxAuthTries 2" >> /etc/ssh/sshd_config
	echo "MaxSessions 2" >> /etc/ssh/sshd_config
	echo "PermitRootLogin no" >> /etc/ssh/sshd_config
	echo "PermitUserEnvironment no" >> /etc/ssh/sshd_config
	echo "PermitTunnel no" >> /etc/ssh/sshd_config
	echo "PrintLastLog yes" >> /etc/ssh/sshd_config
	echo "StrictModes yes" >> /etc/ssh/sshd_config
	echo "TCPKeepAlive no" >> /etc/ssh/sshd_config
	echo "UseDNS no" >> /etc/ssh/sshd_config
	echo "X11Forwarding no" >> /etc/ssh/sshd_config
	echo "AllowAgentForwarding no" >> /etc/ssh/sshd_config
	echo "PermitEmptyPasswords no" >> /etc/ssh/sshd_config
#	echo "PubkeyAuthentication\ yes" >> /etc/ssh/sshd_config		# uncomment if use pubkey
#	echo "PasswordAuthentication\ no" >> /etc/ssh/sshd_config		# uncomment if use pubkey
	echo "Protocol 2" >> /etc/ssh/sshd_config
#	echo "HashKnownHosts yes" >> /etc/ssh/sshd_config
	echo "Banner /etc/issue.net" >> /etc/ssh/sshd_config
	echo "DebianBanner no" >> /etc/ssh/sshd_config		# redhat breaks
#	echo "VersionAddendum none" >> /etc/ssh/sshd_config	# redhat fix, uncomment if rh distro
	# CIS benchmarks start
	echo "Protocol 2" >> /etc/ssh/sshd_config
	echo "HostbasedAuthentication no" >> /etc/ssh/sshd_config
	echo "PermitUserEnvironment no" >> /etc/ssh/sshd_config
	echo "Ciphers chacha20-poly1305@openssh.com,aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com" >> /etc/ssh/sshd_config
	echo "MACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256,hmac-sha2-512" >> /etc/ssh/sshd_config
#	echo "KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256" >> /etc/ssh/sshd_config	# invalid kex algos included, pending fix!
	
	# canary bit
	echo "# autodit-ssh was here..." >> /etc/ssh/sshd_config # && echo "debug autodit-ssh" # debug

	systemctl restart sshd.service && { echo "DONE"; echo "Manually configure allow user/group in sshd_config!"; } || { echo "Something went wrong starting SSH server.";  echo "Check sshd_config with \"sudo sshd -T\"."; }
	
elif [ ! -e /etc/ssh/sshd_config ]; then
	echo "SSHd not installed..."
else
	echo "SSHd already configured..."
fi

echo ""
#cont
}



kernel_harden(){	# lynis and CIS benchmarks
# Kernel hardening
echo "Hardening kernel: "

# backup config
cp -na /etc/sysctl.conf /etc/sysctl.conf_bak

# restrict perms
chown root:root /etc/sysctl.conf /etc/sysctl.conf_bak
chmod og-wx /etc/sysctl.conf /etc/sysctl.conf_bak


# deduplication canary
if [ -e /etc/sysctl.conf -a "$(head -n1 /etc/sysctl.conf)" != "# autodit was here..." ]; then

	# comment out relevant settings

	# lynis configurations
	sed -e 's/^\(fs\.protected_hardlinks\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(fs\.protected_symlinks\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(fs\.suid_dumpable\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.core_uses_pid\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.ctrl-alt-del\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.dmesg_restrict\)/\#\1/gI' -i /etc/sysctl.conf
# /etc/modprobe.d/iwlwifi.conf

	sed -e 's/^\(kernel\.kptr_restrict\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(kernel\.randomize_va_space\)/\#\1/gI' -i /etc/sysctl.conf	# CIS benchmarks too
	sed -e 's/^\(kernel\.sysrq\)/\#\1/gI' -i /etc/sysctl.conf
#	sed -e 's/^\(kernel\.yama.ptrace_scope\)/\#\1/gI' -i /etc/sysctl.conf	#BREAKS VMWARE! REEEE!
	sed -e 's/^\(net\.ipv4.conf\.all\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.conf\.all\.bootp_relay\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.log_martians\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.mc_forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.proxy_arp\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.rp_filter\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.conf\.all\.send_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.log_martians\)/\#\1/gI' -i /etc/sysctl.conf
#	sed -e 's/^\(net\.ipv4\.icmp_echo_ignore_broadcasts\)/\#\1/gI' -i /etc/sysctl.conf	# both lynis and CIS, disabled bc remote network
	sed -e 's/^\(net\.ipv4\.icmp_ignore_bogus_error_responses\)/\#\1/gI' -i /etc/sysctl.conf

	sed -e 's/^\(net\.ipv4\.tcp_syncookies\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.tcp_timestamps\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_source_route\)/\#\1/gI' -i /etc/sysctl.conf
	#lynis configurations end

	sed -e 's/^\(net\.ipv4\.ip_forward\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.forwarding\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.send_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.all\.secure_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.secure_redirects\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.all\.accept_ra\)/\#\1/gI' -i /etc/sysctl.conf
	
	sed -e 's/^\(net\.ipv6\.conf\.default\.accept_ra\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.rp_filter\)/\#\1/gI' -i /etc/sysctl.conf
	
	#disable ipv6	(this part is optional)
	sed -e 's/^\(net\.ipv6\.conf\.all\.disable_ipv6\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv6\.conf\.default\.disable_ipv6\)/\#\1/gI' -i /etc/sysctl.conf
	sed -e 's/^\(net\.ipv4\.conf\.default\.rp_filter\)/\#\1/gI' -i /etc/sysctl.conf

	# change settings through append
	echo "" >> /etc/sysctl.conf
	echo "" >> /etc/sysctl.conf
	echo "# autodit kernel hardening configurations:" >> /etc/sysctl.conf

	# lynis configurations
	echo "fs.protected_hardlinks = 1" >> /etc/sysctl.conf
	echo "fs.protected_symlinks = 1" >> /etc/sysctl.conf
	echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf
	echo "kernel.core_uses_pid = 1" >> /etc/sysctl.conf
	echo "kernel.ctrl-alt-del = 0" >> /etc/sysctl.conf
	echo "kernel.dmesg_restrict = 1" >> /etc/sysctl.conf

	echo "kernel.kptr_restrict = 2" >> /etc/sysctl.conf
	echo "kernel.randomize_va_space = 2" >> /etc/sysctl.conf	# CIS benchmarks too
	echo "kernel.sysrq = 0" >> /etc/sysctl.conf
#	echo "kernel.yama.ptrace_scope = 2" >> /etc/sysctl.conf	#BREAKS VMWARE! REEEE!
	echo "net.ipv4.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.accept_source_route = 0" >> /etc/sysctl.conf

	echo "net.ipv4.conf.all.bootp_relay = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.log_martians = 1" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.mc_forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.proxy_arp = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.rp_filter = 1" >> /etc/sysctl.conf

	echo "net.ipv4.conf.all.send_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.log_martians = 1" >> /etc/sysctl.conf
#	echo "net.ipv4.icmp_echo_ignore_broadcasts = 1" >> /etc/sysctl.conf	# both lynis and CIS, disabled bc remote network
	echo "net.ipv4.icmp_ignore_bogus_error_responses = 1" >> /etc/sysctl.conf

	echo "net.ipv4.tcp_syncookies = 1" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_timestamps = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_source_route = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.accept_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.accept_source_route = 0" >> /etc/sysctl.conf
	# lynis configuration end

	echo "net.ipv4.ip_forward = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.forwarding = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.send_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.all.secure_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv4.conf.default.secure_redirects = 0" >> /etc/sysctl.conf
	echo "net.ipv6.conf.all.accept_ra = 0" >> /etc/sysctl.conf
	
	echo "net.ipv6.conf.default.accept_ra = 0" >> /etc/sysctl.conf 
	echo "net.ipv4.conf.default.rp_filter = 1" >> /etc/sysctl.conf 
	
	#disable ipv6	(optional)
	echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
	echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf

	# reload sysctl.conf
	sysctl -p

	# deduplication canary
	sed -i /etc/sysctl.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' && echo "DONE" # && echo "debug kernel-harden" #debug

else
	echo "Kernel was hardened by autodit. Append check additional configurations manually!"

fi

echo ""
#cont
}



network_harden(){	# deprecated
# Network hardening
echo "Hardneing network settings: "

cp -na /etc/host.conf /etc/host.conf_bak

# deduplication canary
if [ -e /etc/host.conf -a "$(head -n1 /etc/host.conf)" != "# autodit was here..." ]; then

	echo "nospoof on" >> /etc/host.conf
	# deduplication canary
	sed -i /etc/host.conf -e '1i\\#\ autodit\ was\ here\.\.\.\n' && echo "DONE" # && echo "debug host.conf" #debug

else
	echo "Network settings were hardened by autodit. Append check additional configurations manually!"

fi
echo ""
#cont
}



grub_harden(){
# harden grub bootloader
echo "Hardening grub: "

# harden grub.conf permissions
chown root:root /boot/grub*/*.cfg
chmod og-rwx /boot/grub*/*.cfg




# deduplication canary
if [ "$(head -n2 /etc/grub.d/40_custom)" != "# autodit was here..." ]; then
	
	# add superuser and password
	echo "Adding grub2 superuser (root) and password:"
	
	# generate grub2 password:
	grub-mkpasswd-pbkdf2|tee -a ./localpw		# redhat breaks, grub2-mkpasswd-pbkdf2
	pwHash="$(tail -n+3 localpw |sed -e 's/.*grub/grub/')"
	rm ./localpw
	echo "set superusers=\"root\"" >> /etc/grub.d/40_custom
	echo "password_pbkdf2 root $pwHash" >> /etc/grub.d/40_custom
	echo "DONE"
	echo ""
	
	# protect linux recovery modes
	echo "Password protecting linux-recovery menuentries:"
	# Debug comment
	#echo "sed -i /etc/grub.d/40_custom -E -e 's/(\t)(menuentry)(.*)(\{.*)/\1\2\3--users \"\" \4/g'" >> /etc/grub.d/40_custom
	# BROKEN! Requires FIX!
	echo DONE
	echo ""

	# update grub configuration
	echo "Updating grub2 configuration file:"	# redhat breaks, no update-grub2
	update-grub2
	echo DONE
	echo ""

	# canary bit
	sed -i /etc/grub.d/40_custom -e '2i\\#\ autodit\ was\ here\.\.\.\n' # && echo "debug grub" #debug

else

	echo "grub2 password was added by autodit..."

fi
echo ""
#cont
}



#firefox_harden(){
	# harden firefox configurations
	
##cont
#}



#libreoffice_harden(){

##cont
#}



fs_harden(){		# CIS benchmarks
# disable unused filesystems
echo ""
mp_conf="/etc/modprobe.d/CIS.conf"
# canary
if [ ! -e "$mp_conf" ]; then
	echo "Disabling uncommon/unused filesystems:"
	# disables uncommon/unused filesystems
	echo "install cramfs /bin/true" >> "$mp_conf"
	echo "install freevxfs /bin/true" >> "$mp_conf"
	echo "install jffs2 /bin/true" >> "$mp_conf"
	echo "install hfs /bin/true" >> "$mp_conf"
	echo "install hfsplus /bin/true" >> "$mp_conf"
	echo "install squashfs /bin/true" >> "$mp_conf"
	echo "install udf /bin/true" >> "$mp_conf"
	# unload current uncommon/unused filesystems
	rmmod cramfs &>/dev/null
	rmmod freevxfs &>/dev/null
	rmmod jffs2 &>/dev/null
	rmmod hfs &>/dev/null
	rmmod hfsplus &>/dev/null
	rmmod squashfs &>/dev/null
	rmmod udf &>/dev/null
	echo "DONE"
	# restrict perms
	chown root:root /etc/modprobe.d/CIS.conf
	chmod og-wx /etc/modprobe.d/CIS.conf
else
	echo "Unused file systems are disabled per CIS benchmarks..."
fi

echo ""
#cont
}



coredump_harden(){	 # CIS benchmarks
# restrict coredumps. suid_dumpable is in kernel_harden
echo "Hardening core dumps:"

# backup configs
cp -na /etc/security/limits.conf /etc/security/limits.conf_bak

# restrict perms
chown root:root /etc/security/limits.conf /etc/security/limits.conf_bak
chmod og-wx /etc/security/limits.conf /etc/security/limits.conf_bak

# canary
if [ -z "$(grep '# Autodit was here...' /etc/security/limits.conf)" ]; then
	# comment out existing setting if any
	sed -E -e 's/([^#](.*(soft|hard).*core.*))/\#\1/' -i /etc/security/limits.conf
	
	# append canary and settings
	echo "" >> /etc/security/limits.conf
	echo '# Autodit was here...' >> /etc/security/limits.conf
	echo "* hard core 0" >> /etc/security/limits.conf
	echo "DONE"
else
	echo "Core dumps already restricted..."
fi
echo ""
#cont
}



mounts_harden(){	# CIS benchmarks
# harden /tmp mount and harden mount options
echo "Hardening mounts:"

# backup configs
cp -na /etc/fstab /etc/fstab_bak

# restrict perms
chown root:root /etc/fstab /etc/fstab_bak
chmod og-wx /etc/fstab /etc/fstab_bak

# canary
if [ -z "$(cat /etc/fstab|grep -i 'mounts_harden')" ]; then
	# comment out existing setting if any
	sed -E -e 's/(.*)(\/tmp)(.*)/#\1\2\3/g' -i /etc/fstab	# /tmp
	sed -E -e 's/(.*)(\/dev\/shm)(.*)/#\1\2\3/g' -i /etc/fstab
#	sed -E -e 's/(.*)(\/home)(.*)/#\1\2\3/g' -i /etc/fstab			# ****s up system
	
	# append canary and fstab entry
	echo "" >> /etc/fstab
	echo '# Autodit mounts_harden was here...' >> /etc/fstab
#	echo -e "tmpfs\t\t/tmp\t\tdefaults,rw,nosuid,nodev,noexec,relatime\t0\t0" >> /etc/fstab	# noexec breaks apt, use with caution
	echo -e "tmpfs\t\t/tmp\t\ttmpfs\tdefaults,rw,nosuid,nodev,relatime\t0\t0" >> /etc/fstab
	echo -e "tmpfs\t\t/dev/shm\ttmpfs\tdefaults,nodev,nosuid,noexec,rw\t0\t0" >> /etc/fstab
#	echo -e "none\t\t/home\t\tauto\tdefaults,nodev\t0\t0" >> /etc/fstab	# ****s up system
	
	echo "/tmp and /dev/shm DONE"
	
else
	echo "fstab mounts already hardened..."
fi

echo ""
#cont
}



autofs_harden(){	# CIS benchmarks
echo "Disabling autofs: "
# disable automounting filesystems
systemctl stop autofs &>/dev/null && echo "autofs STOPPED" || echo "autofs not installed or something went wrong..."
systemctl disable autofs &>/dev/null && echo "autofs DISABLED" || echo "autofs not installed or something went wrong..."
echo ""
#cont
}



sticky_harden(){	# CIS benchmarks
# enable sticky bit on world writable paths
echo "Setting sticky bit on world writable paths:"
# get world writable paths
wrw_paths="$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null)"
echo "World writable paths w/o sticky bit:"
echo "$wrw_paths"
echo ""
# canary
if [ -n "$wrw_paths" ]; then
	chmod a+t $(echo "$wrw_paths"|tr "\n" " ")
fi
echo "Sticky bit set on all world writable paths."
echo ""
#cont
}



prelink_harden(){	# CIS benchmark
# undo and remove prelink
echo "Undoing prelink:"
# canary
if [ -n "$(dpkg -s prelink|head -n 1|grep -i not\ installed)" ]; then
	# undo prelink
	prelink -ufa &>/dev/null && echo "DONE" || echo "Something went wrong..."
	# purge prelink and execstack
	echo "Purging prelink:"
	apt-get -q -y purge prelink execstack &>/dev/null && echo "DONE" || echo "Something went wrong..."
else
	echo "prelink not installed..."
fi
echo ""
#cont
}



inetd_harden(){		# CIS benchmark
# remove inetd completely
echo "Purging inetd and variants:"
apt-get purge -q -y '.*inetd.*' &>/dev/null && echo "inetd and variants purged." || echo "Something went wrong..."

echo ""
#cont
}



ntp_harden(){		# CIS benchmark
# configure and harden ntp
echo "Installing and configuring NTP:"

apt-get install -q -y ntp &>/dev/null && echo "ntp installed." || echo "Something went wrong..."
# ntp canary
if [ -z "$(cat /etc/ntp.conf | grep -i ntp_harden)" ]; then
	# backup ntp.conf
	cp -na /etc/ntp.conf /etc/ntp.conf_bak
	
	# restrict perms
	chown root:root /etc/ntp.conf /etc/ntp.conf_bak
	chmod og-wx /etc/ntp.conf /etc/ntp.conf_bak
	
	# comment out relevant entries
	sed -E -e 's/^(restrict.*)/#\1/g' -i /etc/ntp.conf
	sed -E -e 's/^(RUNASUSER=.*)/#\1/g' -i /etc/ntp.conf
	
	# appened settings and canary
	echo "" >> /etc/ntp.conf
	echo "# Autodit ntp_harden was here..." >> /etc/ntp.conf
	echo "RUNASUSER=ntp" >> /etc/ntp.conf
	echo "restrict -4 default ignore kod nomodify notrap nopeer noquery" >> /etc/ntp.conf
	echo "restrict -6 default ignore kod nomodify notrap nopeer noquery" >> /etc/ntp.conf

else
	echo "NTP already hardened..."
fi

echo "current ntp.conf servers:"
egrep -E '^server' /etc/ntp.conf |cut -f2 -d\ 

echo ""
#cont
}



service_harden(){	# CIS benchmark
# disable unnecessary services
echo "Disabling unnecessary services:"
systemctl disable avahi-daemon &>/dev/null && echo "avahi-daemon disabled." || echo "Something went wrong when disabling avahi-daemon..."
apt-get -q -y remove '^cups.*' &>/dev/null && echo "cups removed." || echo "Something went wrong removing cups..."
systemctl disable isc-dhcp-server &>/dev/null && echo "isc-dhcp-server disabled." || echo "Something went wrong when disabling isc-dhcp-server..."
systemctl disable isc-dhcp-server6 &>/dev/null && echo "isc-dhcp-server6 disabled." || echo "Something went wrong when disabling isc-dhcp-server6..."
systemctl disable slapd &>/dev/null && echo "slapd disabled." || echo "Something went wrong when disabling slapd..."
systemctl disable nfs-server &>/dev/null && echo "nfs-server disabled." || echo "Something went wrong when disabling nfs-server..."
systemctl disable rpcbind &>/dev/null && echo "rpcbind disabled." || echo "Something went wrong when disabling rpcbind..."
systemctl disable bind9 &>/dev/null && echo "bind9 disabled." || echo "Something went wrong when disabling bind9..."
systemctl disable vsftpd &>/dev/null && echo "vsftpd disabled." || echo "Something went wrong when disabling vsftpd..."
systemctl disable apache2 &>/dev/null && echo "apache2 disabled." || echo "Something went wrong when disabling apache2..."
apt-get purge -q -y 'exim4.*' &>/dev/null && echo "exim4 purged" || echo "Something went wrong when purging exim4..."
systemctl disable smbd &>/dev/null && echo "smbd disabled." || echo "Something went wrong when disabling smbd..."
systemctl disable squid &>/dev/null && echo "squid disabled." || echo "Something went wrong when disabling squid..."
systemctl disable snmpd &>/dev/null && echo "snmpd disabled." || echo "Something went wrong when disabling snmpd..."
# mail agent pending, postfix for now
systemctl disable rsync &>/dev/null && echo "rsync disabled." || echo "Something went wrong when disabling rsync..."
systemctl disable nis &>/dev/null && echo "nis disabled." || echo "Something went wrong when disabling nis..."
apt-get -q -y remove '.*telnetd.*' 'telnet.*server' &>/dev/null  && echo "telnetd removed." || echo "Something went wrong removing telnetd..."
apt-get -q -y remove minidlna minissdpd miniupnpc miniupnpd '^rygel.*' &>/dev/null && echo "upnp services removed" || echo "Something went wrong removing upnp services..."
echo ""

# stop unnecessary services
echo "Stopping unnecessary services:"
systemctl stop avahi-daemon &>/dev/null && echo "avahi-daemon stopped." || echo "Something went wrong when stopping avahi-daemon..."
systemctl stop isc-dhcp-server &>/dev/null && echo "isc-dhcp-server stopped." || echo "Something went wrong when stopping isc-dhcp-server..."
systemctl stop isc-dhcp-server6 &>/dev/null && echo "isc-dhcp-server6 stopped." || echo "Something went wrong when stopping isc-dhcp-server6..."
systemctl stop slapd &>/dev/null && echo "slapd stopped." || echo "Something went wrong when stopping slapd..."
systemctl stop nfs-server &>/dev/null && echo "nfs-server stopped." || echo "Something went wrong when stopping nfs-server..."
systemctl stop rpcbind &>/dev/null && echo "rpcbind stopped." || echo "Something went wrong when stopping rpcbind..."
systemctl stop bind9 &>/dev/null && echo "bind9 stopped." || echo "Something went wrong when stopping bind9..."
systemctl stop vsftpd &>/dev/null && echo "vsftpd stopped." || echo "Something went wrong when stopping vsftpd..."
systemctl stop apache2 &>/dev/null && echo "apache2 stopped." || echo "Something went wrong when stopping apache2..."
systemctl stop smbd &>/dev/null && echo "smbd stopped." || echo "Something went wrong when stopping smbd..."
systemctl stop squid &>/dev/null && echo "squid stopped." || echo "Something went wrong when stopping squid..."
systemctl stop snmpd &>/dev/null && echo "snmpd stopped." || echo "Something went wrong when stopping snmpd..."
systemctl stop rsync &>/dev/null && echo "rsync stopped." || echo "Something went wrong when stopping rsync..."
systemctl stop nis &>/dev/null && echo "nis stopped." || echo "Something went wrong when stopping nis..."

echo""
#cont
}



service_client_harden(){	# CIS benchmarks
echo "Removing unnecessary service clients:"
apt-get -q -y remove nis &>/dev/null && echo "nis removed." || echo "Something went wrong removing nis..."
apt-get -q -y remove '^rsh.*' &>/dev/null && echo "rsh removed." || echo "Something went wrong removing rsh..."
apt-get -q -y remove '^talk.*' '^.talk$' &>/dev/null && echo "talk removed." || echo "Something went wrong removing talk..."
apt-get -q -y remove telnet &>/dev/null && echo "telnet removed." || echo "Something went wrong removing telnet..."
apt-get -q -y remove ldap-utils &>/dev/null && echo "ldap-utils removed." || echo "Something went wrong removing ldap-utils..."
echo ""
#cont
}



tcpwrapper_harden(){	# CIS benchmarks
# install and harden tcp wrappers
echo "Installing tcpd:"
apt-get -q -y remove tcpd &>/dev/null && echo "tcpd installed." || echo "Something went wrong installing tcpd..."
echo ""

# change hosts.allow and hosts.deny permissions
echo "Don't forget to configure hosts.allow and hosts.deny!"
echo "Changing permissions on hosts.allow and hosts.deny:"
chown root:root /etc/hosts.allow /etc/hosts.deny
chmod 644 /etc/hosts.allow /etc/hosts.deny
echo "DONE"

echo ""
#cont
}



netproto_harden(){	# CIS benchmarks
# disable uncommon network protocols
echo "Disabling uncommon network protocols:"
# disables uncommon network protocols
echo "install dccp /bin/true" > /etc/modprobe.d/dccp.conf
echo "install sctp /bin/true" > /etc/modprobe.d/dccp.conf
echo "install rds /bin/true" > /etc/modprobe.d/dccp.conf
echo "install tipc /bin/true" > /etc/modprobe.d/dccp.conf
# unloads uncommon network protocols
rmmod dccp &>/dev/null
rmmod sctp &>/dev/null
rmmod rds &>/dev/null
rmmod tipc &>/dev/null
echo "DONE"

echo ""
#cont
}



cron_harden() {		# CIS benchmarks
# harden cron configuration
echo "Hardening cron:"

# enable cron
systemctl enable cron &>/dev/null && echo "cron enabled." || echo "Something went wrong enabling cron..."

# harden crontab permissions
chown root:root /etc/crontab /etc/cron.d /etc/cron.hourly /etc/cron.daily /etc/cron.weekly /etc/cron.monthly
chmod og-wx /etc/crontab /etc/cron.d
chmod og-w /etc/cron.hourly /etc/cron.daily /etc/cron.weekly /etc/cron.monthly 

# restrict crontab through cron.allow
if [ -e /etc/cron.allow -a -z "$(cat /etc/cron.allow|grep -i cron_harden)" ]; then
	# backup original configs
	cp -na /etc/cron.allow /etc/cron.allow_bak
	
	# canary
elif [ ! -e /etc/cron.allow ]; then
	touch /etc/cron.allow
fi
# canary
if [ -z "$(cat /etc/cron.allow|grep -i cron_harden)" ]; then
	echo "" >> /etc/cron.allow
	echo "" >> /etc/cron.allow
	echo "# Autodit cron_harden was here..." >> /etc/cron.allow && { echo "cron.allow processed."; echo "Manually edit cron.allow to limit cron.";} || echo "cron.allow went wrong..."
else
	echo "cron.allow already processed."
	echo "Manually edit cron.allow to limit cron."
fi

# restrict at through at.allow
if [ -e /etc/at.allow -a -z "$(cat /etc/at.allow|grep -i cron_harden)" ]; then
	# backup original configs
	cp -na /etc/at.allow /etc/at.allow_bak
	
	# canary
elif [ ! -e /etc/at.allow ]; then
	touch /etc/at.allow
fi
# canary
if [ -z "$(cat /etc/at.allow|grep -i cron_harden)" ]; then
	echo "" >> /etc/at.allow
	echo "" >> /etc/at.allow
	echo "# Autodit cron_harden was here..." >> /etc/at.allow && { echo "at.allow processed."; echo "Manually edit at.allow to limit cron.";} || echo "at.allow went wrong..."
else
	echo "at.allow already processed."
	echo "Manually edit at.allow to limit at."
fi

# backup cron.deny
if [ -e /etc/cron.deny ]; then
	cp -na /etc/cron.deny /etc/cron.deny_bak
	rm /etc/cron.deny
fi

# backup at.deny
if [ -e /etc/at.deny ]; then
	cp -na /etc/at.deny /etc/at.deny_bak
	rm /etc/at.deny
fi

# limit perms
chown root:root /etc/cron.allow /etc/at.allow /etc/cron.deny_bak /etc/at.deny_bak &>/dev/null
chmod og-wx /etc/cron.allow /etc/at.allow /etc/cron.deny_bak /etc/at.deny_bak &>/dev/null

echo ""
#cont
}



anacron_harden(){
# harden anacron configuration
echo "Hardening anacron:"
anacron_harden_success="true"

# harden anacron files perms
chown root:root /etc/anacrontab /var/spool/anacron || anacron_harden_success="false"
chmod og-rwx /etc/anacrontab /var/spool/anacron || anacron_harden_success="false"

if [ ! -e /etc/anacrontab  ]; then
	echo "Anacron not installed or anacrontab missing..."
elif [ ! -e /var/spool/anacron ]; then
	echo "DONE, /var/spool/anacron missing though..."
elif [ "$anacron_harden_success" == "false" ]; then
	echo "Something went wrong..."
else
	echo "DONE"
fi

echo ""
#cont
}



systemd_harden(){
# harden systemd configurations
echo "Hardening systemd:"

# change unit file directory perms
chown root:root /etc/systemd/system /run/systemd/system /lib/systemd/system /etc/systemd/user /run/systemd/user /usr/lib/systemd/user
chmod og-rw /etc/systemd/ /run/systemd/ /lib/systemd/
chmod og-rwx /etc/systemd/journald.conf /etc/systemd/coredump.conf /etc/systemd/resolved.conf /etc/systemd/system.conf /etc/systemd/timesyncd.conf /etc/systemd/logind.conf /etc/systemd/user.conf

# completion pending!

echo ""
#cont
}



user_harden(){		# CIS benchmarks
# harden user configuration	//originally harden users...
echo "Hardening user configurations:"

# non-login system accounts
echo "System accounts non-login:"
usermod -s /usr/sbin/nologin sync && echo "DONE user sync" || echo "User sync went wrong..."
usermod -s /usr/sbin/nologin shutdown && echo "DONE user shutdown" || echo "User shutdown went wrong..."
usermod -s /usr/sbin/nologin halt && echo "DONE user halt" || echo "User halt went wrong..."

# default root group
echo "Root group GID=0:"
usermod -g 0 root &>/dev/null && echo "DONE" || echo "Something went wrong..."


echo ""
#cont
}



bash_harden(){		# CIS benchmarks
# harden bash startup configs
echo "Hardening BASH configurations:"
# perms
cp -na /etc/profile /etc/profile_bak
cp -na /etc/bash.bashrc /etc/bash.bashrc_bak
chown root:root /etc/profile /etc/profile_bak /etc/bash.bashrc /etc/bash.bashrc_bak
chmod og-wx /etc/profile /etc/profile_bak /etc/bash.bashrc /etc/bash.bashrc_bak

# interactive login shell
echo "Hardening /etc/profile:"
if [ -z "$(cat /etc/profile|grep -i user_harden)" ]; then
	# canary
	echo "" >> /etc/profile
	echo "" >> /etc/profile
	echo "# Autodit user_harden was here..." >> /etc/profile
	
	bash_harden_profile="true"

	# append config, no comment out bc bash startup execs top down
	echo "umask 027" >> /etc/profile || { echo "umask 027 went wrong..."; bash_harden_profile="false"; }
	echo "TMOUT=600" >> /etc/profile || { echo "timeout went wrong..."; bash_harden_profile="false"; }
	
	if [ "$bash_harden_profile" == "true" ]; then
		echo "DONE"
	fi
else
	echo "/etc/profile already hardened"
fi

# interactive non-login shell
echo "Hardening /etc/bash.bashrc:"
if [ -z "$(cat /etc/bash.bashrc|grep -i user_harden)" ]; then
	# canary
	echo "" >> /etc/bash.bashrc
	echo "" >> /etc/bash.bashrc
	echo "# Autodit user_harden was here..." >> /etc/bash.bashrc
	
	bash_harden_bashrc="true"

	# append config, no comment out bc bash startup execs top down
	echo "umask 027" >> /etc/bash.bashrc || { echo "umask 027 went wrong..."; bash_harden_bashrc="false"; }
	echo "TMOUT=600" >> /etc/bash.bashrc || { echo "umask 027 went wrong..."; bash_harden_bashrc="false"; }
	
	if [ "$bash_harden_bashrc" == "true" ]; then
		echo "DONE"
	fi
else
	echo "/etc/bash.bashrc already hardened"
fi

echo ""
#cont
}



su_harden(){		# CIS benchmarks
# require pam_wheel for su access
echo "Hardening su:"
cp -na /etc/pam.d/su /etc/pam.d/su_bak

# harden perms
chown root:root /etc/pam.d/su /etc/pam.d/su_bak
chmod og-wx /etc/pam.d/su /etc/pam.d/su_bak

if [ -z "$(cat /etc/pam.d/su|grep -i su_harden)" ]; then
	#canary
	echo "" >> /etc/pam.d/su
	echo "" >> /etc/pam.d/su
	echo "# Autodit su_harden was here..." >> /etc/pam.d/su
	
	su_harden_bit="true"
	
	# appened rule infront of common auth rules
	sed -E -e 's/(@include.*common-auth.*)/\nauth required pam_wheel.so\n\1/gi' -i /etc/pam.d/su || { echo "Something went wrong..."; su_harden_bit="false"; }
	
	if [ "$su_harden_bit" == "true" ]; then
		echo "DONE"
	fi
else
	echo "su already hardened..."
fi

echo ""
#cont
}



perms_harden(){		# CIS benchmarks
# harden file permissions
echo "Hardening system file perms:"
perms_harden_sys_bit="true"
chown root:root /etc/passwd || perms_harden_sys_bit="false"
chmod 644 /etc/passwd || perms_harden_sys_bit="false"
chown root:shadow /etc/shadow || perms_harden_sys_bit="false"
chmod o-rwx,g-wx /etc/shadow || perms_harden_sys_bit="false"
chown root:root /etc/group || perms_harden_sys_bit="false"
chmod 644 /etc/group || perms_harden_sys_bit="false"
chown root:shadow /etc/gshadow || perms_harden_sys_bit="false"
chmod o-rwx,g-rw /etc/gshadow || perms_harden_sys_bit="false"
chown root:root /etc/passwd- || perms_harden_sys_bit="false"
chmod u-x,go-wx /etc/passwd- || perms_harden_sys_bit="false"
chown root:shadow /etc/shadow- || perms_harden_sys_bit="false"
chmod o-rwx,g-rw /etc/shadow- || perms_harden_sys_bit="false"
chown root:root /etc/group- || perms_harden_sys_bit="false"
chmod u-x,go-wx /etc/group- || perms_harden_sys_bit="false"
chown root:shadow /etc/gshadow- || perms_harden_sys_bit="false"
chmod o-rwx,g-rw /etc/gshadow- || perms_harden_sys_bit="false"
if [ "$perms_harden_sys_bit" == "true" ]; then
	echo "DONE"
else
	echo "Something went wrong..."
fi

# harden user home directory perms
echo "Hardening user home directory perms:"
chmod o-rwx,g-w /home/*/ && echo "DONE" || echo "Something went wrong..."

}



auditd_harden(){
# install auditd
echo "Installing auditd:"
if [ -z "$(dpkg -s auditd|head -n 1|grep -i not\ installed)" ]; then
	apt-get install -q -y auditd &>/dev/null echo "Auditd installed." || echo "Something went wrong..."
else
	echo "auditd already installed."
fi

# backup configs
cp -na /etc/audit/auditd.conf /etc/audit/auditd.conf_bak || echo "Something went wrong with auditd.conf backup..."
cp -na /etc/audit/auditd.conf /etc/audit/auditd.conf_bak || echo "Something went wrong with auditd.conf backup..."
cp -na /etc/default/grub /etc/default/grub_bak
cp -na /etc/audit/audit.rules /etc/audit/audit.rules_bak


# restrict perms
chown root:root /etc/audit/ /var/log/audit/ /etc/default/grub /etc/audit/audit.rules
chmod 750 /etc/audit/ /var/log/audit/
chmod og-wx /etc/default/grub /etc/default/grub_bak
chmod og-wx /etc/audit/audit.rules /etc/audit/audit.rules_bak




# configure auditd
# auditd.conf
echo  "Configuring auditd.conf:"
if [ -z "$(cat /etc/audit/auditd.conf|grep auditd_harden)" ]; then
	
	
	
	# successbit
	auditd_harden_auditconf_bit="true"
	
	# canary
	echo "" >> /etc/audit/auditd.conf || auditd_harden_auditconf_bit="false"
	echo "" >> /etc/audit/auditd.conf || auditd_harden_auditconf_bit="false"
	echo "# Autodit auditd_harden was here..." >> /etc/audit/auditd.conf || auditd_harden_auditconf_bit="false"
	
	# comment out relevant settings
	sed -E -e 's/[^#]?(max_log_file_action.*)/#\1/gi' -i /etc/audit/auditd.conf || auditd_harden_auditconf_bit="false"

	# appened releveant settings
	echo "max_log_file_action = keep_logs" >> /etc/audit/auditd.conf

	# check for failure
	if [ "$auditd_harden_auditconf_bit" == "false" ]; then
		echo "Something went wrong configuring auditd.conf..."
	fi
else
	echo "auditd.conf already hardened..."
fi

# auditd preboot events
echo "Configuring auditd preboot looging:"
if [ -z "$(cat /etc/default/grub|grep -i auditd_harden)" ]; then
	
	# successbit
	auditd_harden_preboot_bit="true"
	
	# canary
	echo "" >> /etc/default/grub || auditd_harden_preboot_bit="false"
	echo "" >> /etc/default/grub || auditd_harden_preboot_bit="false"
	echo "# Autodit auditd_harden was here..." >> /etc/default/grub || auditd_harden_preboot_bit="false"
	
	# comment out relevant settings
	#sed -E -e 's/([^#]?GRUB_CMDLINE_LINUX[^_])/#\1/' -i /etc/default/grub || auditd_harden_preboot_bit="false"

	# appened releveant settings
	#echo "max_log_file_action = keep_logs" >> /etc/default/grub || auditd_harden_preboot_bit="false"

	# comment and append entry on original
	sed -E -e 's/([^#]?GRUB_CMDLINE_LINUX[^_]\"?.*)(\")/# autodit appended settings\n\1 audit=1\2/' -i /etc/default/grub || auditd_harden_preboot_bit="false"

	# update grub
	update-grub &>/dev/null && echo ""|| 

	# check for failure
	if [ "$auditd_harden_preboot_bit" == "false" ]; then
		echo "Something went wrong configuring grub auditd settings..."
	fi
else
	echo "auditd preboot logging is already configured..."
fi

# auditd rules				COMMENTED OUT BC INCOMPLETE! REEEE
echo "Hardening auditd rules: "
if [ ! -e /etc/audit/rules.d/40-audit_harden_canary.rules ]; then

	# successbit
	auditd_harden_rules_bit="true"

	# canary
	touch /etc/audit/rules.d/40-audit_harden_canary.rules || auditd_harden_rules_bit="false"
	
	# appened rule files to rules.d
	# CIS_timedate
	echo "-a always,exit -F arch=b32 -S adjtimex -S settimeofday -S stime -k time-change" >> /etc/audit/rules.d/41-CIS_timedate.rules	# <----------|
	echo "-a always,exit -F arch=b32 -S clock_settime -k time-change" >> /etc/audit/rules.d/41-CIS_timedate.rules				#            |
	if [ -n "$(arch|grep x86_64)" ]; then													#            |
		echo "-a always,exit -F arch=b64 -S adjtimex -S settimeofday -k time-change" >> /etc/audit/rules.d/41-CIS_timedate.rules	# one option |
		echo "-a always,exit -F arch=b64 -S clock_settime -k time-change" >> /etc/audit/rules.d/41-CIS_timedate.rules			#            |
	fi																	#            |
	echo "-w /etc/localtime -p wa -k time-change" >> /etc/audit/rules.d/41-CIS_timedate.rules						# <----------|
	# CIS_ugsp (Users, Groups, Shadow, Password)
	echo "-w /etc/group -p wa -k identity" >> /etc/audit/rules.d/42-CIS_ugsp.rules								# <----------|
	echo "-w /etc/passwd -p wa -k identity" >> /etc/audit/rules.d/42-CIS_ugsp.rules								#            |
	echo "-w /etc/gshadow -p wa -k identity" >> /etc/audit/rules.d/42-CIS_ugsp.rules							# one option |
	echo "-w /etc/shadow -p wa -k identity" >> /etc/audit/rules.d/42-CIS_ugsp.rules								#            |
	echo "-w /etc/security/opasswd -p wa -k identity" >> /etc/audit/rules.d/42-CIS_ugsp.rules						# <----------|
	# CIS_network
	echo "-a always,exit -F arch=b32 -S sethostname -S setdomainname -k system-locale" >> /etc/audit/rules.d/43-CIS_network.rules
	if [ -n "$(arch|grep x86_64)" ]; then
		echo "-a always,exit -F arch=b64 -S sethostname -S setdomainname -k system-locale" >> /etc/audit/rules.d/43-CIS_network.rules
	fi
	echo "-w /etc/issue -p wa -k system-locale" >> /etc/audit/rules.d/43-CIS_network.rules
	echo "-w /etc/issue.net -p wa -k system-locale" >> /etc/audit/rules.d/43-CIS_network.rules
	echo "-w /etc/hosts -p wa -k system-locale" >> /etc/audit/rules.d/43-CIS_network.rules
	echo "-w /etc/sysconfig/network -p wa -k system-locale" >> /etc/audit/rules.d/43-CIS_network.rules
	# CIS_mac (44-CIS_mac.rules)
	#	COMPLETION PENDING!
	#	SELINUX AND APPARMOR
	# CIS_login
	echo "-w /var/log/faillog -p wa -k logins" >> /etc/audit/rules.d/45-CIS_login.rules
	echo "-w /var/log/lastlog -p wa -k logins" >> /etc/audit/rules.d/45-CIS_login.rules
	echo "-w /var/log/tallylog -p wa -k logins" >> /etc/audit/rules.d/45-CIS_login.rules
	# CIS_session
	echo "-w /var/run/utmp -p wa -k session" >> /etc/audit/rules.d/46-CIS_session.rules
	echo "-w /var/log/wtmp -p wa -k logins" >> /etc/audit/rules.d/46-CIS_session.rules
	echo "-w /var/log/btmp -p wa -k logins" >> /etc/audit/rules.d/46-CIS_session.rules
	# CIS_permmod
	echo "-a always,exit -F arch=b32 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> /etc/audit/rules.d/47-CIS_permmod.rules
	echo "-a always,exit -F arch=b32 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> /etc/audit/rules.d/47-CIS_permmod.rules
	echo "-a always,exit -F arch=b32 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> /etc/audit/rules.d/47-CIS_permmod.rules
	if [ -n "$(arch|grep x86_64)" ]; then
		echo "-a always,exit -F arch=b64 -S chmod -S fchmod -S fchmodat -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> /etc/audit/rules.d/47-CIS_permmod.rules
		echo "-a always,exit -F arch=b64 -S chown -S fchown -S fchownat -S lchown -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> /etc/audit/rules.d/47-CIS_permmod.rules
		echo "-a always,exit -F arch=b64 -S setxattr -S lsetxattr -S fsetxattr -S removexattr -S lremovexattr -S fremovexattr -F auid>=1000 -F auid!=4294967295 -k perm_mod" >> /etc/audit/rules.d/47-CIS_permmod.rules
	fi
	
	# generate auditd rules
	augenrules

	# check for failure
	if [ "$auditd_harden_rules_bit" == "false" ]; then
		echo "Something went wrong configuring auditd rules..."
	fi
else
	echo "auditd rules already hardened..."
	echo "manually check if other rules need to be implemented!"
fi


systemctl reload auditd &>/dev/null && echo "Auditd rules reloaded." || echo "Something went wrong reloading auditd rules..."


echo "Reference timestamps:"
echo -e "Local:\t$(date +%b\ %d\ %k:%m:%S)"
echo -e "UTC:\t$(date -u +%b\ %d\ %k:%m:%S)"
echo -e "ISO:\t$(date +%s)"

}



# method to invoke above methods, leave at bottom!
driver
