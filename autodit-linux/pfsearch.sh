#!/bin/bash

# NOTE TO DEVS:

# Credits:

# Table of contents: (search to jump!)
#	Prohibited File Search
#		home directory media file search
#		root directory media file search
#		full system search (wannacrypt pattern)

# (c) 2019-2020, salted-Dev-null, All Rights Reserved



#------ Prohibited File Search ------
echo "------ Prohibited File Search ------"
echo ""


# home directory media file search
echo "Searching for prohibited files in /home directory."
echo "See pfsearch-home.log:"
find /home -iregex '.*AIF\|.*M3U\|.*TXT\|.*M4A\|.*MID\|.*MP3\|.*MPA\|.*RA\|.*WAV\|.*WMA\|.*3G2\|.*3GP\|.*ASF\|.*ASX\|.*AVI\|.*FLV\|.*M4V\|.*MOV\|.*MP4\|.*MPG\|.*RM\|.*SRT\|.*SWF\|.*VOB\|.*WMV\|.*BMP\|.*GIF\|.*JPG\|.*PNG\|.*PSD\|.*TIF\|.*YUV\|.*GAM\|.*SAV\|.*TORRENT\|.*WEBM\|.*OGG|.*MPEG|.*FLAC\|.*JPEG'  &>pfsearch-home.log && echo "DONE"
echo "DONE SEARCH" >> pfsearch-home.log
chmod 666 ./pfsearch-home.log


# root directory media file search
echo "Searching for prohibited files in root directory."
echo "See pfsearch-root.log:"
find / -iregex '.*AIF\|.*M3U\|.*TXT\|.*M4A\|.*MID\|.*MP3\|.*MPA\|.*RA\|.*WAV\|.*WMA\|.*3G2\|.*3GP\|.*ASF\|.*ASX\|.*AVI\|.*FLV\|.*M4V\|.*MOV\|.*MP4\|.*MPG\|.*RM\|.*SRT\|.*SWF\|.*VOB\|.*WMV\|.*BMP\|.*GIF\|.*JPG\|.*PNG\|.*PSD\|.*TIF\|.*YUV\|.*GAM\|.*SAV\|.*TORRENT\|.*WEBM\|.*OGG|.*MPEG|.*FLAC\|.*JPEG'  &>pfsearch-root.log && echo "DONE"
echo "DONE SEARCH" >> pfsearch-root.log
chmod 666 ./pfsearch-root.log


# full system search (wannacrypt pattern)
echo "Searching for prohibited files using wannacrypt pattern."
echo "See pfsearch-full.log:"
find / -iname "*.doc" -o -iname "*.docx" -o -iname "*.xls"\
-o -iname "*.xlsx" -o -iname "*.ppt" -o -iname "*.pptx"\
-o -iname "*.pst" -o -iname "*.ost" -o -iname "*.msg"\
-o -iname "*.eml" -o -iname "*.vsd" -o -iname "*.vsdx"\
-o -iname "*.txt" -o -iname "*.csv" -o -iname "*.rtf"\
-o -iname "*.123" -o -iname "*.wks" -o -iname "*.wk1"\
-o -iname "*.pdf" -o -iname "*.dwg" -o -iname "*.onetoc2"\
-o -iname "*.snt" -o -iname "*.jpeg" -o -iname "*.jpg"\
-o -iname "*.docb" -o -iname "*.docm" -o -iname "*.dot"\
-o -iname "*.dotm" -o -iname "*.dotx" -o -iname "*.xlsm"\
-o -iname "*.xlsb" -o -iname "*.xlw" -o -iname "*.xlt"\
-o -iname "*.xlm" -o -iname "*.xlc" -o -iname "*.xltx"\
-o -iname "*.xltm" -o -iname "*.pptm" -o -iname "*.pot"\
-o -iname "*.pps" -o -iname "*.ppsm" -o -iname "*.ppsx"\
-o -iname "*.ppam" -o -iname "*.potx" -o -iname "*.potm"\
-o -iname "*.edb" -o -iname "*.hwp" -o -iname "*.602"\
-o -iname "*.sxi" -o -iname "*.sti" -o -iname "*.sldx"\
-o -iname "*.sldm" -o -iname "*.sldm" -o -iname "*.vdi"\
-o -iname "*.vmdk" -o -iname "*.vmx" -o -iname "*.gpg"\
-o -iname "*.aes" -o -iname "*.ARC" -o -iname "*.PAQ"\
-o -iname "*.bz2" -o -iname "*.tbk" -o -iname "*.bak"\
-o -iname "*.tar" -o -iname "*.tgz" -o -iname "*.gz"\
-o -iname "*.7z" -o -iname "*.rar" -o -iname "*.zip"\
-o -iname "*.backup" -o -iname "*.iso" -o -iname "*.vcd"\
-o -iname "*.bmp" -o -iname "*.png" -o -iname "*.gif"\
-o -iname "*.raw" -o -iname "*.cgm" -o -iname "*.tif"\
-o -iname "*.tiff" -o -iname "*.nef" -o -iname "*.psd"\
-o -iname "*.ai" -o -iname "*.svg" -o -iname "*.djvu"\
-o -iname "*.m4u" -o -iname "*.m3u" -o -iname "*.mid"\
-o -iname "*.wma" -o -iname "*.flv" -o -iname "*.3g2" \
-o -iname "*.mkv" -o -iname "*.3gp" -o -iname "*.mp4"\
-o -iname "*.mov" -o -iname "*.avi" -o -iname "*.asf"\
-o -iname "*.mpeg" -o -iname "*.vob" -o -iname "*.mpg"\
-o -iname "*.wmv" -o -iname "*.fla" -o -iname "*.swf"\
-o -iname "*.wav" -o -iname "*.mp3" -o -iname "*.sh"\
-o -iname "*.class" -o -iname "*.jar" -o -iname "*.java"\
-o -iname "*.rb" -o -iname "*.asp" -o -iname "*.php"\
-o -iname "*.jsp" -o -iname "*.brd" -o -iname "*.sch"\
-o -iname "*.dch" -o -iname "*.dip" -o -iname "*.pl"\
-o -iname "*.vb" -o -iname "*.vbs" -o -iname "*.ps1"\
-o -iname "*.bat" -o -iname "*.cmd" -o -iname "*.js"\
-o -iname "*.asm" -o -iname "*.h" -o -iname "*.pas"\
-o -iname "*.cpp" -o -iname "*.c" -o -iname "*.cs"\
-o -iname "*.suo" -o -iname "*.sln" -o -iname "*.ldf"\
-o -iname "*.mdf" -o -iname "*.ibd" -o -iname "*.myi"\
-o -iname "*.myd" -o -iname "*.frm" -o -iname "*.odb"\
-o -iname "*.dbf" -o -iname "*.db" -o -iname "*.mdb"\
-o -iname "*.accdb" -o -iname "*.sql" -o -iname "*.sqlitedb"\
-o -iname "*.sqlite3" -o -iname "*.asc" -o -iname "*.lay6"\
-o -iname "*.lay" -o -iname "*.mml" -o -iname "*.sxm"\
-o -iname "*.otg" -o -iname "*.odg" -o -iname "*.uop"\
-o -iname "*.std" -o -iname "*.sxd" -o -iname "*.otp"\
-o -iname "*.odp" -o -iname "*.wb2" -o -iname "*.slk"\
-o -iname "*.dif" -o -iname "*.stc" -o -iname "*.sxc"\
-o -iname "*.ots" -o -iname "*.ods" -o -iname "*.3dm"\
-o -iname "*.max" -o -iname "*.3ds" -o -iname "*.uot"\
-o -iname "*.stw" -o -iname "*.sxw" -o -iname "*.ott"\
-o -iname "*.odt" -o -iname "*.pem" -o -iname "*.p12"\
-o -iname "*.csr" -o -iname "*.crt" -o -iname "*.key"\
-o -iname "*.pfx" -o -iname "*.der" -o -iname "*.flac"\
-o -iname "*.m4a" -o -iname "*.ogg" -o -iname "" 2>/dev/null |sort >pfsearch-full.log 2>/dev/null && echo "DONE"

echo "DONE SEARCH" >> pfsearch-full.log

chmod 666 ./pfsearch-full.log


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Prohibited File Search End ------
