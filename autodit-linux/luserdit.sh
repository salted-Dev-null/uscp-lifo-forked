#!/bin/bash

# NOTE TO DEVS:
# COMPLETION PENDING!
# DOCUMENTATION PENDING!

# (c) 2019-2020, salted-Dev-null, All Rights Reserved


# Table of contents:
#	Check root
#	Define variables		# **** this, too many to keep track
#	Get active users
#	Program loop
#		remove user loop
#		de-admin loop
#		add user loop
#		escalate user loop
#	Add user loop	(DEPRECATED)
#	Escalate loop	(DEPRECATED)



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



# Define variables

# loop conditions
programRun="true"
removeRun="true"
deadminRun="true"
addRun="true"
exitRun="true"

# user manipulation
listFields="null"	# for activeUsers parsing
activeUsers="null"
fieldLength="null"	# loop removal specific
# debug comment
#authorizedUsers=$(cat $1|tr "\n" ","|sed -e 's/[,]$//g')
currentUser='null'

# remove-user-loop runtime
ruprompt="null"
rmUsers="null"
rmInputCheck="null"


# Program loop
# ------ PROGRAM LOOP START ------
while [ "$programRun" == "true" ]; do

#debug
#echo "Program loop start"

# update loop conditions
programRun="true"
deadminRun="true"
removeRun="true"
addRun="true"
exitRun="true"


#------ REMOVE-USER LOOP START ------
while [ "$removeRun" == "true" ]; do

	#debug
	#echo "remove-user loop start"

	# input cheking step
	rmInputCheck="true"
	while [ "$rmInputCheck" == "true" ]; do

		#debug
		#echo "rmInputchecking loop start"

		read -p "Do you want to remove users? (Y/N): " ruprompt
		if [ -z "$ruprompt" -o -z "$(echo $ruprompt|egrep -i '^n|^y')" ]; then
			unset ruprompt
			echo "Invalid input."
		else
			rmInputCheck="false"
		fi
	done

	# skip to next loop if no
	if [ -n "$ruprompt" -a -n "$(echo $ruprompt|egrep -i '^n')" ]; then
		removeRun="false"


	# prompt and remove users if yes
	elif [ -n "$ruprompt" -a -n "$(echo $ruprompt|egrep -i '^y')" ]; then
		echo ""


		# Get/update active users
		activeUsers=$(sudo cat /etc/passwd|sed -e "/nologin/d" -e "/false/d" -e "/sync/d" -e "/true/d" -e "" |cut -f1 -d":"|sort|tr "\n" ",")
		listFields=$(echo $activeUsers |grep -o ","|wc -l)


		# list active users
		echo "Active Users on server: (command to audit: userdel -r <USER>)"
		for i in `seq 1 $listFields`
		do
			groups $(echo "$activeUsers" |cut -f$i -d",")|sed -e "s/^/$i: /gim"
		done
		echo ""
		
		
		# prompt user removal
		read -p "Which users to remove (numbers/0 to skip): " rmUsers


		# input checking
		if [ -z "$rmUsers" ]; then
			echo "Please enter comma seperated users!"
			exit 1
		else
			# fieldLength input sanitation
			fieldLength=$((0+$(echo "$rmUsers" | sed -e 's/[^,]//g'|wc -m)))
			if [ -z "$(echo $rmUsers|cut -d\, -f $(($fieldLength)))" ]; then
				fieldLength=$(($fieldLength-1))
			fi
		fi
		
		
		# remove users
		for i in `seq 1 $fieldLength`; do
			currentUser="$(echo $activeUsers|cut -f $(echo $rmUsers|cut -f$i -d,) -d',')"

			if [ -n "$currentUser" ]; then

				echo "removing $currentUser"
				deluser --remove-home $currentUser
		
			fi

		done


	fi


echo ""
echo ""
done
#------ REMOVE-USER LOOP END ------



#debug
#echo "deadmin catch start"
#------ DE-ADMIN LOOP START ------
while [ "$deadminRun" == "true" ]; do


	# input cheking step
	daInputCheck="true"
	while [ "$daInputCheck" == "true" ]; do
	
		read -p "Do you want to de-escalate admins? (Y/N): " daprompt
		if [ -z "$daprompt" -o -z "$(echo $daprompt|egrep -i '^n|^y')" ]; then
			unset daprompt
			echo "Invalid input."
		else
			daInputCheck="false"
		fi
	
	done
	
	
	# skip to next loop if no
	if [ -n "$daprompt" -a -n "$(echo $daprompt|egrep -i '^n')" ]; then
		deadminRun=false


	# prompt and remove users if yes
	elif [ -n "$daprompt" -a -n "$(echo $daprompt|egrep -i '^y')" ]; then
		echo ""


		# get/update active users and reset variables
		activeUsers=$(sudo cat /etc/passwd|sed -e "/nologin/d"|sed -e "/false/d" -e "/sync/d"|cut -f1 -d":"|sort|tr "\n" ",")
		listFields=$(echo $activeUsers |grep -o ","|wc -l)
		activeAdmins=""

		
		# get admins from active users
		for i in `seq 1 $listFields`;do
			
			currentUser="$(echo $activeUsers|cut -f$i -d,)"
			
			# append admins to list
			if [ -n "$(groups $currentUser|egrep 'sudo|adm|sudoer|admin|wheel|root')" ]; then
			
				activeAdmins="$activeAdmins,$currentUser"
			
			fi
		
		done
		listFields=$(echo $activeAdmins |grep -o ","|wc -l)
		
		
		# parse activeAdmins
		activeAdmins="$(echo $activeAdmins|tail -c +2)"

				
		#debug
		#echo $activeAdmins

				
		# display active admins
		echo "Active admins (command to audit: deluser <ADMIN> <ADMIN GROUPS>):"
		for i in `seq 1 $listFields`;do
			
			echo "$activeAdmins" |cut -f$i -d","|sed -e "s/^/$i: /gim"
			
		done
		
		
		# prompt admin de-escalation
		read -p "Which admins to de-escalate (numbers/0 to skip): " deAdmins
		
		
		# input checking
		if [ -z "$deAdmins" ]; then
			echo "Please enter comma seperated admins!"
			exit 1
		else
			# fieldLength input sanitation
			fieldLength=$((0+$(echo "$deAdmins" | sed -e 's/[^,]//g'|wc -m)))
			if [ -z "$(echo $deAdmins|cut -d\, -f $(($fieldLength)))" ]; then
				fieldLength=$(($fieldLength-1))
			fi
		fi
		
		
		
		# de-escalate admins
		for i in `seq 1 $fieldLength`; do
			currentUser="$(echo $activeAdmins|cut -f $(echo $deAdmins|cut -f$i -d,) -d',')"

			if [ -n "$currentUser" ]; then

				echo "de-escalating $currentUser"
				deluser $currentUser sudo	2>/dev/null
				deluser $currentUser sudoer	2>/dev/null
				deluser $currentUser adm	2>/dev/null
				deluser $currentUser wheel	2>/dev/null
				deluser $currentUser root	2>/dev/null
				deluser $currentUser admin	2>/dev/null
		
			fi

		done

		
	fi


echo ""
echo ""
done
#------ DE-ADMIN LOOP END ------



#------ ADD USER LOOP START ------
#while [ "$addRun" == "true" ]; do
#
#	# input cheking step
#	adInputCheck="true"
#	while [ "$adInputCheck" == "true" ]; do
#	
#		read -p "Do you want to add users? (Y/N): (NOT FUNCTIONING YEET!)" adprompt
#		if [ -z "$adprompt" -o -z "$(echo $adprompt|egrep -i '^n|^y')" ]; then
#			unset daprompt
#			echo "Invalid input."
#		else
#			adInputCheck="false"
#		fi
#	
#	done
#	
#	# skip to next loop if no
#	if [ -n "$adprompt" -a -n "$(echo $adprompt|egrep -i '^n')" ]; then
#		addRun=false
#	#debug
#	#fi
#	
#	
#	# prompt and add users if yes
#	elif [ -n "$adprompt" -a -n "$(echo $adprompt|egrep -i '^y')" ]; then
#		
#		# check number input
#		adNumCheck="true"
#		while [ "$adNumCheck" == "true" ]; do
#			
#			read -p "How many users do you want to add (numbers/0 to skip): " addNum
#			
#			if [ "$addNum" -lt 0 -o "$addNum" -ge 0 ]; then
#				
#				adNumCheck="false"
#				
#			fi
#			
#		done
#		
#		# loop to add users and fill in details
#		i=0
#		while [ $i -lt $addNum ]; do
#			
#			let i=i+1
#			
#			#debug
#			#echo "added user $i"
#			
#			
#			
#		done
#		
#	fi
#
#
#
#
#
#done
#------ ADD USER LOOP END ------



#------ EXIT LOOP ------
while [ "$exitRun" == "true" ]; do


	# input cheking step
	exitInputCheck="true"
	while [ "$exitInputCheck" == "true" ]; do
	
		read -p "Do you want to exit luserdit.sh? (Y/N): " exitprompt
		if [ -z "$exitprompt" -o -z "$(echo $exitprompt|egrep -i '^n|^y')" ]; then
			unset exitprompt
			echo "Invalid input."
		else
			exitInputCheck="false"
		fi
	
	done
	
	
	# skip to next loop if no
	if [ -n "$exitprompt" -a -n "$(echo $exitprompt|egrep -i '^n')" ]; then
		exitRun="false"


	# prompt and remove users if yes
	elif [ -n "$exitprompt" -a -n "$(echo $exitprompt|egrep -i '^y')" ]; then

		exit
		
	fi


echo ""
echo ""
done




#------ EXIT LOOP END ------



#debug
#echo "Program loop end"
# ------ PROGRAM LOOP END------
done
