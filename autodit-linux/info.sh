#!/bin/bash

# NOTE TO DEVS:

# Credits:

# Table of contents: (search to jump!)
#	General Auditing Tips 
#	Users
#		get users with UID 0
#		list users with admin privileges
#		get passwordless users
#	Networking
#		notes
#		list open TCP ports
#		list established connections
#		list UDP connections
#		list network services
#		firewall status
#	Packages and Commands
#		$PATH variable check
#		malicious alias check
#		apt source check
#		debsums check
#		which/whereis check	(COMPEND)


# (c) 2019-2020, salted-Dev-null, All Rights Reserved



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



#------ General Auditing Tips ------
echo "------ General Auditing Tips ------"
printf "\nREAD company policies and forensics questions first!!!\n"
printf "General streamlined procedure:\n"
printf "Firewalling --> Install av and ids --> User auditing --> \n"
printf "Configure auto updates --> Install Security Updates --> \n\n"
echo "------ End Section ------"
printf "\n\n\n"
#------ General Auditing Tips End------



#------  Users ------
echo "------ Users ------"
echo ""


# get users with UID 0
echo "Users with UID 0:"
cut -f1,3 -d":" /etc/passwd|grep ":0"
echo ""


# get active users, filter results and display privileges
# declare variables here!!!
activeUsers=null
listFields=0
currentUser=null

echo "Active Users on server: (command to audit: userdel -r <USER>)"

activeUsers=$(cat /etc/passwd|sed -e "/nologin/d"|sed -e "/false/d" -e "/sync/d"|cut -f1 -d":"|sort|tr "\n" ",")

listFields=$(echo $activeUsers |grep -o ","|wc -l)

for i in `seq 1 $listFields`
do
	groups $(echo "$activeUsers" |cut -f$i -d",")
done
echo ""


# list users with admin privileges
echo "Users with admin privileges: (command to audit: deluser <user> <admin group>)"

for i in `seq 1 $listFields`
do
        currentUser=$(groups $(echo "$activeUsers" |cut -f$i -d","))
	echo "$currentUser"|grep "admin\|adm\|sudo\|wheel\|root"|cut -f1 -d " "

done
echo ""


# get passwordless users
npUsers=$(passwd -Sa| grep " NP " |cut -f1 -d" "|sort|tr "\n" ",")
listFields=$(echo $npUsers |grep -o ","|wc -l)
currentUser=null

echo "Users with no passwords on server: (Command to audit: usermod -l)"

for i in `seq 1 $listFields`
do
	groups $(echo "$npUsers" |cut -f$i -d",")
done
echo ""


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------  Users End ------



#------ Networking ------
echo "------ Open ports and listening services ------"
echo ""


# notes
echo "notes: when in doubt, GOOGLE!"
echo "command to audit: apt purge <non-critical services> -y"
echo ""


# list open TCP ports
echo "Open ports (behind firewall): "
netstat -antpul|grep -i listen
printf "\n"


# list established connections
echo "Established and other connections: "
netstat -antpul|grep -i 'tcp'|sed -e "/LISTEN/d"
printf "\n"


# list UDP connections
echo "UDP connections: "
netstat -antpul|grep -i "udp"
echo""


# list network services
echo "Possible networked services: (refers above)"
#debug comment-out
#netstat -antpulW|tail -n +3|sed 's/.*\///g'|sort|uniq
networkedServices="$(netstat -antpulW)"
parsedServices="$(echo "$networkedServices"|tail -n +3|grep '/'|sed 's/.*\///g'|sort|uniq)"
echo "$parsedServices"
echo ""


# firewall status
echo "UFW status:"
ufw status verbose 2>/dev/null  || echo "Something went wrong..."


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Networking End ------



#------ Packages and Commands ------
echo "------ Packages and Commands ------"
echo ""


# $PATH variable check
echo "\$PATH varible directories:"
echo "$PATH"|tr ':' "\n"
echo ""


# malicious alias check
echo "Shell aliases: (check for malicious)"
alias
echo ""


# apt source check
echo "Please manually check apt sources! (:n and :p to swtich between files in less)"
echo "Press enter to continue:"
read
more /etc/apt/sources.list /etc/apt/sources.list.d/*


# debsums check
# debug comment
#if [ ! "$(apt search debsums|cut -f1 -d\ )" == "i" ]; then	# check debsums inst
#	echo "Installing debsums:"
#	apt install debsums -y &>/dev/null && echo DONE
#else
#	echo "debsums is installed."
#fi

echo "Checking package intergrity: (see debsums-fail.log)"
apt clean 	&>/dev/null
apt update	&>/dev/null
apt-get --reinstall -d install `debsums -l`	&>/dev/null
debsums -ac -p /var/cache/apt/archives &> debsums-fail.log
chmod 666 debsums-fail.log
echo DONE
echo ""


echo ""
echo "------ End Section ------"
printf "\n\n\n"
#------ Packages and Commands End ------

