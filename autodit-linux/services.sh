#!/bin/bash

# NOTE TO DEVS:
# REFER TO:
#	INIT CHEATSHEET: https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet
#	chkconfig replacement: https://askubuntu.com/questions/2263/chkconfig-alternative-for-ubuntu-server
#	init systems: https://www.tecmint.com/best-linux-init-systems/
#	Upstart specs and !!!VARIOUS INITS!!!: http://upstart.ubuntu.com/misc/upstart.pdf
#	init/SystemV vs Upstart vs SystemD: https://fossbytes.com/systemd-vs-sys-v-vs-upstart/

# Credits:

# Table of contents: (search to jump!)
#	init/SystemV
#		detect init/SystemV
#		inittab
#		otherstuff(obselete?)
#	init Scripts
#		runlevels eneabled scripts
#		init.d scripts
#		init configuration files
#	Upstart ?			(COMPEND)
#	Systemd
#	atd				(COMPEND)


# (c) 2019-2020, salted-Dev-null, All Rights Reserved



# check root
if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi



# install prerequisite
apt-get install systemd --reinstall -y



#------ init/SystemV ------
echo "------ init/SystemV ------"
echo ""


# detect init/SystemV
echo "Auditing init/SystemV: "
if [ -x /etc/inittab ]; then
	echo "init/SystemV detected."
	initExists="true"
else
	echo "init/SystemV not detected."
fi


if [ "$initExists" == "true" ]; then
	
	# inittab
	echo "inittab contents: "
	echo "Press enter to continue: "
	read
	less /etc/inittab
	
	# other stuff (not tested due to init being outdated...)
	# debug/other codeblock here...
		
fi
echo ""


echo "------ init/SystemV End------"
printf "\n\n\n"
#------ init/SystemV End ------



#------ init Scripts ------
echo "------ init Scripts ------"
echo ""

# list services
echo "Init/SysV services status:"
service --status-all
echo ""

# runlevels eneabled scripts
echo "Enabled Scripts at runlevels:"
echo "Rerun {ls -l /etc/rc*.d/*} for more details."
echo "Use update-rc.d <service> <command|args> to manage."
echo ""

# runlevel 0
echo "runlevel 0 (poweroff.target):"
ls --color=auto -lA /etc/rc0.d/S*
echo ""

# runlevel 1
echo "runlevel 1 (rescue.target):"
ls --color=auto -lA /etc/rc1.d/S*
echo ""

# runlevel 2
echo "runlevel 2 (multi-user.target):"
ls --color=auto -lA /etc/rc2.d/S*
echo ""

# runlevel 3
echo "runlevel 3 (multi-user.target):"
ls --color=auto -lA /etc/rc3.d/S*
echo ""

# runlevel 4
echo "runlevel 4 (multi-user.target):"
ls --color=auto -lA /etc/rc4.d/S*
echo ""

# runlevel 5
echo "runlevel 5 (graphical.target):"
ls --color=auto -lA /etc/rc5.d/S*
echo ""

# runlevel 6
echo "runlevel 6 (reboot.target):"
ls --color=auto -lA /etc/rc6.d/S*
echo ""

# runlevel S
echo "runlevel S (rescue.target? unsure.):"
ls --color=auto -lA /etc/rcS.d/S*
echo ""
echo ""


# init.d scripts
echo "/etc/init.d scripts: "
echo "Check suspicious scripts with {less /etc/init.d/suspicious-script}"
ls --color=auto -A /etc/init.d/
echo ""


# init configuration files
echo "/etc/init/ configuration files "
echo "Check suspicious configs with {less /etc/init/suspicious-config}:"
ls --color=auto -A /etc/init/
echo ""


echo "------ init Scripts End------"
printf "\n\n\n"
#------ init Scripts End------



# ----comment
# required? confused af rn, will append later.
# comment ----

#------ Upstart ------
#echo "------ Upstart ------"
#echo ""

#echo "------ Upstart End------"
#------ Upstart End ------



#------ Systemd ------
echo "------ Systemd ------"
echo ""

# services
echo "Systemd service unit files."
echo "Located in /lib/systemd/system/<name>.service"
echo "-- and --"
echo "/etc/systemd/system/<name>.service"
echo "Services:"
systemctl list-unit-files --type service



echo "------ Systemd End ------"
printf "\n\n\n"
#------ Systemd End ------
