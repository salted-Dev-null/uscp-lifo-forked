# USCP-LIFO REPO
****
### Description:
Congrats! You have stumbled upon team LIFO's (Last In First Out) gitlab repo! [Here](https://gitlab.com/salted-Dev-null/uscp-lifo-forked/) is where we develop and deposit our scripts and tools. All tools and scripts are custom made for CPXI and should be able to handle general auditing scenarios. Good luck, have fun and don't forget:

**SPEED IS KEY!**
****
### Toolkits included:
Autodit -- Linux

unamed yet -- Windows
****
### Credits:
Autodit was made by Tibo Cheung (salted-Dev-null).
Windows scripts were made by Ian Ertzinger.
